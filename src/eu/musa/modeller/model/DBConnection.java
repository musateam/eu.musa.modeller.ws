//*************************************************
//* Fichero:	DBConnection.java					*
//* Autor:    Carmen Palacios  					          *
//* Creado:	  July 2016               						*
//* Descripci�n:	Clase que implementa la conexi�n*
//* a BD                                          *
//*************************************************
package eu.musa.modeller.model;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;


public class DBConnection
{
	public Connection con=null;
	public boolean local = true;


//////////////////////////////////////////////////////////////////////
// Constructores  
	public DBConnection() {
		
		Properties properties = new java.util.Properties();  
		try {
			if (local)
				properties.load(this.getClass().getResourceAsStream("dbconfig_local.properties"));			
			else
				properties.load(this.getClass().getResourceAsStream("dbconfig_openshift.properties"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}  

        String driver= (String) properties.get("driver");
        String url= (String) properties.get("url");
        String user= (String) properties.get("user");
        String pass= (String) properties.get("pass");
	
      try {
          //MCP
          Class.forName(driver).newInstance();
      }
      catch (Exception e) {
          System.out.println("No es capaz de cargar el driver.");
          System.err.println("No es capaz de cargar el driver.");
          e.printStackTrace();
          return ;
      }


      try {    	  
    	  con = DriverManager.getConnection(url+"?user="+user+"&password="+pass);    	  
      }
      catch (Exception e)
      {
            if (e instanceof SQLException)
            {
              SQLException E = (SQLException) e;
              System.out.println("SQLException: " + E.getMessage());
              System.out.println("SQLState:     " + E.getSQLState());
              System.out.println("VendorError:  " + E.getErrorCode());
              System.out.flush();
            }
            else
              System.out.println("Exception: " + e.getMessage());
      }
      
      if(con == null)
        System.out.println("Error en conexion a BDD");
	}


/////////////////////////////////////////////////////////////////////////////
// Metodos p�blicos
//*********************************************************************
//* Nombre
//*		showException
//* Descripcion
//*		Imprime en pantalla el mensaje de la excepci�n indicada
//* Parametros
//*		E : excepci�n
//*********************************************************************
	public void showException(SQLException E) {
			System.out.println("SQLException: " + E.getMessage());
			System.out.println("SQLState:     " + E.getSQLState());
			System.out.println("VendorError:  " + E.getErrorCode());
      E.printStackTrace();
      System.out.flush();
        }


}