package eu.musa.modeller.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Agent")
// JAX-RS supports an automatic mapping from JAXB annotated class to XML and JSON
public class Agent {
	private String id;
    private String name;
    private String description;
    
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
    
    
    
}