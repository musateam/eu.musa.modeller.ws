//*************************************************
//* Fichero:	FuncGenerales.java						      *
//* Autor:    Carmen Palacios  					          *
//* Creado:	  July 2016               						*
//* Descripci�n:	Clase que implementa las funciones generales  *
//*************************************************
package eu.musa.modeller.model;

import java.sql.*;
import java.util.*;
import java.text.*;
import java.io.*;

public class FuncGenerales
{

//////////////////////////////////////////////////////////////////////
// Constructores  
  public FuncGenerales()
  {
  }

/////////////////////////////////////////////////////////////////////////////
// Metodos p�blicos
//*********************************************************************
//* Nombre
//*		obtenerFechaSQL
//* Descripcion
//*		Transforma la fecha indicada en formato SQL (p.e., 2001-01-01 00:00:00)
//* Parametros
//*		fechaCal : fecha
//*********************************************************************
  static public String obtenerFechaSQL(Calendar fechaCal)
  {
     String fechaStr="";
     String fechaSQL="2001-01-01 00:00:00";
     if(fechaCal == null)
     {
       return fechaSQL;
     }
     SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
     java.util.Date fecha = fechaCal.getTime();
     fechaSQL = sdf.format(fecha);

     return fechaSQL;
  }

//*********************************************************************
//* Nombre
//*		obtenerFechaUI
//* Descripcion
//*		Transforma la fecha indicada en formato para el usuario (p.e., 01-01-2001 00:00:00)
//* Parametros
//*		fechaCal : fecha
//*********************************************************************
  static public String obtenerFechaUI(Calendar fechaCal)
  {
     String fechaStr="";
     String fechaUI="";
     if(fechaCal == null)
     {
       return fechaUI;
     }
     SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
     java.util.Date fecha = fechaCal.getTime();
     fechaUI = sdf.format(fecha);

     return fechaUI;
  }


//*********************************************************************
//* Nombre
//*		showException
//* Descripcion
//*		Imprime en pantalla el mensaje de la excepci�n indicada
//* Parametros
//*		e : excepcion
//*********************************************************************
	public static void showException(ArrayIndexOutOfBoundsException e) {
			System.out.println("Indice del array fuera de rango");
      System.out.println("ArrayIndexOutOfBoundsException: " + e.getMessage());
			e.printStackTrace();
  }
  
//*********************************************************************
//* Nombre
//*		showException
//* Descripcion
//*		Imprime en pantalla el mensaje de la excepci�n indicada
//* Parametros
//*		e : excepcion
//*********************************************************************
  public static void showException(Exception e) {
      System.out.println("Exception: " + e.getMessage());
			e.printStackTrace();
  }
  
  

//*********************************************************************
//* Nombre
//*		getIcono
//* Descripcion
//*		Carga un icono de BD a un fichero temporal
//* Parametros
//*		fname : nombre del fichero a crear con la imagen
//*		imagen : imagen
//*********************************************************************
  public static File getIcono(String fname, Blob imagen)
  {
      File res=null;
      String doutput="";
      String foutput;
      //doutput = DatosGenerales.getDirImagenes();
      foutput = doutput+fname;

      //File tmpi = new File(foutput);
      if (fname == null || fname.compareTo("") == 0)
         return res;
     
      try {  
          //Creo un buffer de 1M
          byte buffer[] = new byte[1*1024*1024];      
      
          // setup the streams
          InputStream entrada = imagen.getBinaryStream();
          //ByteArrayOutputStream salida = new ByteArrayOutputStream();    
          OutputStream salida = new FileOutputStream(foutput);

          // process blob          
          int Datos;
          while ( (Datos = entrada.read(buffer,0,buffer.length)) != -1 )      
          {
            salida.write(buffer,0,Datos);
          }
          salida.flush();
          salida.close();
          entrada.close();
          
          res = new File(foutput);
      }catch(Exception e)
      {
            e.printStackTrace();
            if (e instanceof FileNotFoundException)
                System.out.println("No se ha encontrado el fichero");
            else if ( e instanceof IOException)
                System.out.println("Error de entrada/salida");
            else if (e instanceof UnsupportedEncodingException)
                System.out.println("No soporta esta codificaci�n");
      }
      
      return res;      
  }


//*********************************************************************
//* Nombre
//*		setIcono
//* Descripcion
//*		Carga un icono en un fichero a memoria
//* Parametros
//*		fich : fichero con la imagen
//*********************************************************************
  public static ByteArrayInputStream setIcono(File fich)
  {
      ByteArrayInputStream res= null;
      
      if (fich == null || fich.exists() == false)
        return res;     
  
      try {
            //Creo un buffer de 1M
            byte buffer[] = new byte[1*1024*1024];      

            //Creo el flujo de entrada desde el fichero hasta el buffer
            FileInputStream entrada = new FileInputStream(fich);
            //Creo el flujo de salida
            ByteArrayOutputStream salida = new ByteArrayOutputStream();            

            int total=0, Datos;
            while ( (Datos = entrada.read(buffer,0,buffer.length)) != -1 )
            {
                salida.write(buffer,0,Datos);
                total += Datos;
            }
            salida.flush();
            
            byte resBuf[]  = salida.toByteArray();
            res = new ByteArrayInputStream(resBuf);

            salida.close();
            entrada.close();
      }catch(Exception e)
      {
            e.printStackTrace();
            if (e instanceof FileNotFoundException)
                System.out.println("No se ha encontrado el fichero");
            else if ( e instanceof IOException)
                System.out.println("Error de entrada/salida");
            else if (e instanceof UnsupportedEncodingException)
                System.out.println("No soporta esta codificaci�n");
      }
      
      return res;
  }


//*********************************************************************
//* Nombre
//*		getImagen
//* Descripcion
//*		Devuelve un fichero que puede encontarse el cualquier 
//*   directorio: Alojamientos,..
//* Parametros
//*		fname : nombre del fichero a crear con la imagen
//*		clase : tipo de instancia (p.e., Incluocio.Alojamiento)
//*********************************************************************
  public static File getImagen(String fich, String clase)
  {
     String doutput="";
     String foutput;
     File tmp=null;

     /*
     if(clase.compareTo("Incluocio.Alojamiento") == 0)     
        doutput = DatosGenerales.getDirAlojamientos();
      */            
     foutput = doutput+fich;

     File tmpi = new File(foutput);
     if (fich.compareTo("") == 0 || tmpi.exists() == false)
       return tmp;     
     else
       return tmpi;
  }

  

/****
//*********************************************************************
//* Nombre
//*		getImagen
//* Descripcion
//*		Copia un fichero con una imagen en el directorio Imagenes
//*   Las imagenes son hasta 3M  
//* Parametros
//*		fname : nombre del fichero a crear con la imagen
//*		clase : tipo de instancia (p.e., Incluocio.Alojamiento)
//*********************************************************************
  public static File getImagen(String fich, String clase)
  {
     String dinput="", doutput;
     String finput, foutput;
     File tmp=null;
     
     if(clase.compareTo("Incluocio.Alojamiento") == 0)
     {
        dinput = DatosGenerales.getDirAlojamientos();       
     }
     doutput = DatosGenerales.getDirImagenes();
     finput = dinput+fich;
     foutput = doutput+fich;

     File tmpi = new File(finput);
     if (fich.compareTo("") == 0 || tmpi.exists() == false)
       return tmp;     
     tmpi=null;
     try 
     {
            //Defino un buffer de 3M
            byte[] buffer = new byte[3*1024*1024];
           
            //defino un flujo de entrada (de la b.d. al buffer)
            //InputStream flujo_bytes = datos.getBinaryStream();
            InputStream flujo_entrada = new FileInputStream(finput);
            //Devuelve el n� total de bytes leidos en el buffer.
            int total_bytes = flujo_entrada.read(buffer);
            //Obtenemos el directorio de salida. Si no existe lo creamos
            File tmp2 = new File(doutput);
            tmp2.mkdirs();
            //creo un flujo de entrada al dispositivo q nos mostrar�
            //el fichero
            OutputStream flujo_salida = new FileOutputStream(foutput);
            //escribo del buffer al dispositivo mediante el flujo
            flujo_salida.write(buffer,0,total_bytes);
            //cierro los flujos de salida y entrada.
            flujo_salida.close();
            flujo_entrada.close();
            
            tmp = new File(foutput);
      }catch (Exception e)
      {
            System.out.println("Fichero="+fich);      
            e.printStackTrace();      
            if (e instanceof FileNotFoundException)
                System.out.println("No se ha encontrado el fichero");
            else if ( e instanceof IOException)
                System.out.println("Error de entrada/salida");
            else if (e instanceof UnsupportedEncodingException)
                System.out.println("No soporta esta codificaci�n");
      }     
      
     return tmp;
  }
***/

//*********************************************************************
//* Nombre
//*		Concatenar
//* Descripcion
//*		Devuelve un String con el formato Texto1,Texto2,...
//* Parametros
//*		arr : array con los textos a concatenar
//*********************************************************************
  public static String Concatenar(String[] arr)
  {
    String tmp="";
    try {
        if(arr.length == 0)
        {
           // no hay ningun elemento
           return tmp;
        }
        
        // si hay algun elemento...
        String token="";
        for (int cont=0; cont < arr.length; cont++)
        {
            token=arr[cont].trim();                                                    
       
            if (cont == 0)
              tmp = token;
            else
              tmp = tmp + "," + token ;
        }

    } catch (ArrayIndexOutOfBoundsException e) {
	      FuncGenerales.showException(e) ;
	  }
    
    return tmp;
  }  


//*********************************************************************
//* Nombre
//*		Concatenar
//* Descripcion
//*		Devuelve un String con el formato Texto1,Texto2,...
//* Parametros
//*		vect : Vector con los textos a concatenar
//*********************************************************************
  public static String Concatenar(Vector vect)
  {
    String tmp="";
    try {
        if(vect.size() == 0)
        {
           // no hay ningun elemento
           return tmp;
        }
        
        // si hay algun elemento...
        String token="";
        for (int cont=0; cont < vect.size(); cont++)
        {
            token=((String )vect.elementAt(cont)).trim();                                                    
       
            if (cont == 0)
              tmp = token;
            else
              tmp = tmp + "," + token ;
        }

    } catch (Exception e) {
	      FuncGenerales.showException(e) ;
	  }
    
    return tmp;
  }  

//*********************************************************************
//* Nombre
//*		generarID
//* Descripcion
//*		Devuelve un String eliminando caracteres acentuados o no permitidos
//* Parametros
//*********************************************************************
   public static String generarID(String fuente)
    {
      String forbiddenChars = "()\\_-.";
      String allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
      int s, s2;

      String source = fuente;
      source=source.replaceAll(" ","");
      String source2 = "", temp;
      for(int i=0;i<source.length();i++)
      {
         temp = "" + source.substring(i, i+1);
         s = forbiddenChars.indexOf(temp);
         s2 = allowedChars.indexOf(temp);
         if(Character.isLetter(source.charAt(i)) && s ==-1 && s2 != -1) 
         {
            source2 += source.charAt(i);
         }
      }
      source=source2;
      
      return source;
    }
  
}
    

	
  

   

