//*************************************************
//* Fichero:	ManageMCAppDB.java						        *
//* Autor:    Carmen Palacios  					          *
//* Creado:	  July 2016               						*
//* Descripci�n:	
//*************************************************
package eu.musa.modeller.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.StringJoiner;
import java.util.Vector;

import eu.musa.modeller.ws.utils.MCApp;

public class ManageMCAppDB {
	String mcapplication = "";
	String version = "";
	String camel = "";
	String camelXMI = "";

	//////////////////////////////////////////////////////////////////////
	// Constructores
	public ManageMCAppDB() {
	}

	/////////////////////////////////////////////////////////////////////////////
	// Metodos p�blicos
	// *********************************************************************
	
	/** Returns a MCApp object encapsulating data from mu_mcapp table
	 * @param mcAppID
	 * @return
	 */
	public MCApp getMCApp(Long mcAppID) {
		MCApp obj = null;
		DBConnection DBCon = null;
		try {
			DBCon = new DBConnection();
			Statement stmt = DBCon.con.createStatement();

			String str;
			str = " SELECT mu_mcapp.MCApplication, mu_mcapp.MCAVersion, " + " mu_mcapp.Camel, " + " mu_mcapp.Date "
					+ " FROM mu_mcapp" + " WHERE mu_mcapp.id= " + mcAppID;

			ResultSet rs = stmt.executeQuery(str);

			int tmpInt, tmpInt2;
			String tmp, notasTmp;
			if (rs.next()) {
				obj = new MCApp();
				obj.setMCApplication(rs.getString("mu_mcapp.MCApplication"));
				obj.setMCAVersion(rs.getString("mu_mcapp.MCAVersion"));
				obj.setCamel(rs.getString("mu_mcapp.Camel"));
				java.util.Date fecha = rs.getDate("mu_mcapp.Date");
				Time hora = rs.getTime("mu_mcapp.Date");
				if (fecha != null && hora != null) {
					int horas = Integer.parseInt(hora.toString().substring(0, 2));
					int minutos = Integer.parseInt(hora.toString().substring(3, 5));
					int segundos = Integer.parseInt(hora.toString().substring(6, 8));
					Calendar fechaAlta = Calendar.getInstance(); // init
																	// currentDate
					fechaAlta.setTime(fecha);
					fechaAlta.set(Calendar.HOUR_OF_DAY, horas);
					fechaAlta.set(Calendar.MINUTE, minutos);
					fechaAlta.set(Calendar.SECOND, segundos);
					obj.setDate(fechaAlta.getTime());
				} else
					obj.setDate(null);
			}
			stmt.close();
			DBCon.con.close();
			DBCon.con = null;
		} catch (SQLException excepcion) {
			DBCon.showException(excepcion);
		}

		return obj;
	} // fin

	/**
	 * Returns the model in camel format given an app Id
	 * 
	 * @param mcAppID
	 * @return
	 */
	public String getAppCamel(Long mcAppID) {
		String strCamel = "";
		DBConnection DBCon = null;

		try {
			DBCon = new DBConnection();
			Statement stmt = DBCon.con.createStatement();

			String str;
			str = " SELECT musa_modeller.mu_mcapp.Camel from musa_modeller.mu_mcapp WHERE mu_mcapp.id=" + mcAppID;

			ResultSet rs = stmt.executeQuery(str);

			int tmpInt, tmpInt2;
			String tmp, notasTmp;
			if (rs.next()) {
				strCamel = rs.getString("mu_mcapp.Camel");
			}
			stmt.close();
			DBCon.con.close();
			DBCon.con = null;
		} catch (SQLException excepcion) {
			DBCon.showException(excepcion);
		}

		return strCamel;
	} // fin

	/**
	 * Returns the model in xmi format given an app Id
	 * 
	 * @param mcAppID
	 * @return
	 */
	public String getAppCamelXML(Long mcAppID) {
		String strCamelXML = "";
		DBConnection DBCon = null;

		try {
			DBCon = new DBConnection();
			Statement stmt = DBCon.con.createStatement();

			String str;
			str = " SELECT musa_modeller.mu_mcapp.CamelXMI from musa_modeller.mu_mcapp WHERE mu_mcapp.id=" + mcAppID;

			ResultSet rs = stmt.executeQuery(str);

			int tmpInt, tmpInt2;
			String tmp, notasTmp;
			if (rs.next()) {
				strCamelXML = rs.getString("mu_mcapp.CamelXMI");
			}
			stmt.close();
			DBCon.con.close();
			DBCon.con = null;
		} catch (SQLException excepcion) {
			DBCon.showException(excepcion);
		}

		return strCamelXML;
	} // fin

	// *********************************************************************
	// * Nombre
	// * getAppValidationReport
	// * Descripcion
	// *
	// * Parametros
	// *
	// *********************************************************************
	public String getAppValidationReport(Long mcAppID) {
		String strReport = "UNKNOWN";
		DBConnection DBCon = null;

		try {
			DBCon = new DBConnection();
			Statement stmt = DBCon.con.createStatement();

			String str;
			str = " SELECT musa_modeller.mu_mcapp.CamelReport, musa_modeller.mu_mcapp.StatusCamelReport from musa_modeller.mu_mcapp WHERE mu_mcapp.id="
					+ mcAppID;

			ResultSet rs = stmt.executeQuery(str);

			if (rs.next()) {
				int tmpInt2;
				String tmpStr;
				tmpStr = rs.getString("mu_mcapp.CamelReport");
				tmpInt2 = rs.getInt("mu_mcapp.StatusCamelReport");
				if (tmpInt2 == 0)
					strReport = tmpStr;
			}
			stmt.close();
			DBCon.con.close();
			DBCon.con = null;
		} catch (SQLException excepcion) {
			DBCon.showException(excepcion);
		}

		return strReport;
	} // fin

	/** Returns the error code corresponding to model validation
	 * @param mcAppID
	 * @return
	 */
	public int getAppErrorsValidationReport(Long mcAppID) {
		int iReport = -1;
		DBConnection DBCon = null;

		try {
			DBCon = new DBConnection();
			Statement stmt = DBCon.con.createStatement();

			String str;
			str = " SELECT musa_modeller.mu_mcapp.ErrorsCamelReport, musa_modeller.mu_mcapp.StatusCamelReport  from musa_modeller.mu_mcapp WHERE mu_mcapp.id="
					+ mcAppID;

			ResultSet rs = stmt.executeQuery(str);

			if (rs.next()) {
				int tmpInt, tmpInt2;
				tmpInt = rs.getInt("mu_mcapp.ErrorsCamelReport");
				tmpInt2 = rs.getInt("mu_mcapp.StatusCamelReport");
				if (tmpInt2 == 0)
					iReport = tmpInt;
			}
			stmt.close();
			DBCon.con.close();
			DBCon.con = null;
		} catch (SQLException excepcion) {
			DBCon.showException(excepcion);
		}

		return iReport;
	} // fin

	// *********************************************************************
	// * Nombre
	// * getMCApps
	// * Descripcion
	// *
	// * Parametros
	// *
	// *********************************************************************
	// obtener
	public Vector getMCApps() {
		DBConnection DBCon = null;
		Vector Lista = new Vector();
		MCApp obj = null;

		try {
			DBCon = new DBConnection();
			Statement stmt = DBCon.con.createStatement();

			String str;
			str = " SELECT mu_mcapp.id,mu_mcapp.MCApplication, mu_mcapp.MCAVersion, " + " mu_mcapp.Camel, "
					+ " mu_mcapp.Date " + " FROM mu_mcapp";
			str = str + " ORDER BY mu_mcapp.MCApplication ASC ";

			ResultSet rs = stmt.executeQuery(str);

			while (rs.next()) {
				obj = new MCApp();
				obj.setId(rs.getLong("mu_mcapp.id"));
				obj.setMCApplication(rs.getString("mu_mcapp.MCApplication"));
				obj.setMCAVersion(rs.getString("mu_mcapp.MCAVersion"));
				obj.setCamel(rs.getString("mu_mcapp.Camel"));
				java.util.Date fecha = rs.getDate("mu_mcapp.Date");
				Time hora = rs.getTime("mu_mcapp.Date");
				if (fecha != null && hora != null) {
					int horas = Integer.parseInt(hora.toString().substring(0, 2));
					int minutos = Integer.parseInt(hora.toString().substring(3, 5));
					int segundos = Integer.parseInt(hora.toString().substring(6, 8));
					Calendar fechaAlta = Calendar.getInstance(); // init
																	// currentDate
					fechaAlta.setTime(fecha);
					fechaAlta.set(Calendar.HOUR_OF_DAY, horas);
					fechaAlta.set(Calendar.MINUTE, minutos);
					fechaAlta.set(Calendar.SECOND, segundos);
					obj.setDate(fechaAlta.getTime());
				} else
					obj.setDate(null);

				Lista.add(obj);
			}
			stmt.close();
			DBCon.con.close();
			DBCon.con = null;
		} catch (SQLException excepcion) {
			DBCon.showException(excepcion);
		}

		return Lista;
	}

	/**
	 * Inserts a new record to mu_mcapp table
	 * 
	 * @param mcapplicationStr
	 * @param versionStr
	 * @param camelStr
	 * @return
	 */
	public Long addMCApp(String mcapplicationStr, String versionStr, String camelStr) {
		DBConnection DBCon = null;

		mcapplication = mcapplicationStr;
		version = versionStr;
		camel = camelStr;
		try {
			DBCon = new DBConnection();
			Statement stmt = DBCon.con.createStatement();

			removeWhiteSpaces();
			if (validate() == false)
				return (long) -1;

			// la fecha de alta es currentdate
			Calendar rightNow = Calendar.getInstance();
			String fechaAltaStr = FuncGenerales.obtenerFechaSQL(rightNow);

			String str;
			str = "INSERT INTO mu_mcapp VALUES('" + mcapplication + "','" + version + "','" + camel + "','',"
					+ (long) -1 + ",'" + fechaAltaStr + "')";
			stmt.executeUpdate(str);
			stmt.close();
			DBCon.con.close();
			DBCon.con = null;
		} catch (Exception excepcion) {
			if (excepcion instanceof SQLException)
				FuncGenerales.showException(excepcion);
			// DBCon.showException ((SQLException)excepcion);
			else
				FuncGenerales.showException(excepcion);
			return (long) -2;
		}
		return (long) 0; // obtener ID???
	}

	/** Updates camel value in mu_mcapp table given the value and the app ID
	 * @param mcAppID
	 * @param camelStr
	 * @return
	 */
	public boolean updateCamelofMCApp(Long mcAppID, String camelStr) {
		DBConnection DBCon = null;
		camel = camelStr;
		try {
			DBCon = new DBConnection();
			Statement stmt = DBCon.con.createStatement();
			removeWhiteSpaces();
			if (validate() == false)
				return false;
			String str;
			str = "UPDATE mu_mcapp SET Camel='" + camel + "'" + " WHERE mu_mcapp.id= " + mcAppID;
			stmt.executeUpdate(str);
			stmt.close();
			DBCon.con.close();
			DBCon.con = null;
		} catch (Exception excepcion) {
			if (excepcion instanceof SQLException)
				FuncGenerales.showException(excepcion);			
			else
				FuncGenerales.showException(excepcion);
			return false;
		}
		return true;
	}

	/** Removes white spaces contained in some variables
	 * 
	 */
	public void removeWhiteSpaces() {
		mcapplication = mcapplication.trim();
		mcapplication = mcapplication.replaceAll("'", "''");
		mcapplication = mcapplication.replaceAll("�", "''");

		version = version.trim();
		version = version.replaceAll("'", "''");
		version = version.replaceAll("�", "''");

		camel = camel.trim();
		camel = camel.replaceAll("'", "''");
		camel = camel.replaceAll("�", "''");

		camelXMI = camelXMI.trim();
		camelXMI = camelXMI.replaceAll("'", "''");
		camelXMI = camelXMI.replaceAll("�", "''");

	}

	/** Checks if the mcapplication is equal to ""
	 * @return
	 */
	public boolean validate() {
		if (mcapplication.compareTo("") == 0)
			return false;
		return true;
	}

	/**
	 * Returns if the table contains a record for an application given its id
	 * 
	 * @param mcAppID
	 * @return
	 */
	public String checkIfAppExists(Long mcAppID) {
		String strResult = "false";
		DBConnection DBCon = null;
		int count = 0;
		try {
			DBCon = new DBConnection();
			Statement stmt = DBCon.con.createStatement();

			String str;
			str = " select COUNT(*) AS count from musa_modeller.mu_mcapp WHERE mu_mcapp.id=" + mcAppID;

			ResultSet rs = stmt.executeQuery(str);

			if (rs.next()) {
				count = rs.getInt("count");
			}

			if (count > 0) {
				strResult = "true";
			}

			stmt.close();
			DBCon.con.close();
			DBCon.con = null;
		} catch (SQLException excepcion) {
			DBCon.showException(excepcion);
		}

		return strResult;
	} // fin

	/**
	 * Returns if the table contains a record for an application given its id
	 * 
	 * @param mcAppID
	 * @return
	 */
	public String checkIfAppExistsInAppComponentTable(Long mcAppID) {
		String strResult = "false";
		DBConnection DBCon = null;
		int count = 0;
		try {
			DBCon = new DBConnection();
			Statement stmt = DBCon.con.createStatement();

			String str;
			str = " select COUNT(*) AS count from musa_modeller.mu_app_component WHERE app_id=" + mcAppID;

			ResultSet rs = stmt.executeQuery(str);

			if (rs.next()) {
				count = rs.getInt("count");
			}

			if (count > 0) {
				strResult = "true";
			}

			stmt.close();
			DBCon.con.close();
			DBCon.con = null;
		} catch (SQLException excepcion) {
			DBCon.showException(excepcion);
		}

		return strResult;
	} // fin

	/**
	 * Returns a map containing the relation of all the components used by an
	 * application
	 * 
	 * @param mcAppID
	 * @return
	 */
	public HashMap getComponentsIdsByApplication(Long mcAppID) {
		HashMap componentMap = new HashMap<>();
		DBConnection DBCon = null;
		String strComponentId = "";
		String strComponentName = "";
		int count = 0;
		try {
			DBCon = new DBConnection();
			Statement stmt = DBCon.con.createStatement();

			String str;
			str = " select mu_app_component.component_id, mu_app_component.component_name from mu_app_component where mu_app_component.app_id="
					+ mcAppID;

			ResultSet rs = stmt.executeQuery(str);

			while (rs.next()) {
				strComponentId = rs.getString("component_id");
				strComponentName = rs.getString("component_name");
				componentMap.put(strComponentName, strComponentId);
			}

			stmt.close();
			DBCon.con.close();
			DBCon.con = null;
		} catch (SQLException excepcion) {
			DBCon.showException(excepcion);
		}

		return componentMap;
	} // fin

	/**
	 * Inserts new component codes to mu_app_component table
	 * 
	 * @return
	 */
	public void insertMissingComponentCodes(Long mcAppID, HashMap<Object, Object> componentsToBeInserted) {
		if ((componentsToBeInserted == null) || (componentsToBeInserted.size() == 0))
			return;
		DBConnection DBCon = null;
		try {
			DBCon = new DBConnection();
			Statement stmt = DBCon.con.createStatement();
			String componentName = "";
			String componentId = "";
			String sentence = "";
			StringBuffer strBuffer;
			strBuffer = new StringBuffer(
					"INSERT INTO `mu_app_component` (`app_id`, `component_id`, `component_name`) VALUES ");
			Set<Object> keySet = componentsToBeInserted.keySet();
			Iterator<Object> iterator = keySet.iterator();
			int autoIncrementalId = 0;
			while (iterator.hasNext()) {
				componentName = (String) iterator.next();
				componentId = (String) componentsToBeInserted.get(componentName);
				if (componentId == null) {
					// first time
					if (autoIncrementalId == 0) {
						autoIncrementalId = getMaximunComponentCodeRelatedToAPP(mcAppID) + 1;
					} else {
						autoIncrementalId++;
					}
					componentId = new Integer(autoIncrementalId).toString();
				}
				strBuffer.append("(" + mcAppID + ", " + componentId + ", '" + componentName + "'),");
			}
			sentence = strBuffer.toString();
			if (sentence.endsWith(",")) {
				sentence = sentence.substring(0, sentence.length() - 1);
			}
			sentence += ";";
			stmt.executeUpdate(sentence);
			stmt.close();
			DBCon.con.close();
			DBCon.con = null;
		} catch (SQLException excepcion) {
			DBCon.showException(excepcion);
		}
	}

	private int getMaximunComponentCodeRelatedToAPP(Long mcAppID) {
		int maxComponentCode = 0;
		DBConnection DBCon = null;
		try {
			DBCon = new DBConnection();
			Statement stmt = DBCon.con.createStatement();

			String str;
			str = "select MAX(mu_app_component.component_id) from mu_app_component where mu_app_component.app_id="
					+ mcAppID;

			ResultSet rs = stmt.executeQuery(str);

			if (rs.next()) {
				maxComponentCode = rs.getInt(1);
			}
			stmt.close();
			DBCon.con.close();
			DBCon.con = null;
		} catch (SQLException excepcion) {
			DBCon.showException(excepcion);
		}

		return maxComponentCode;
	} // fin

	/**
	 * Deletes components from mu_app_component table that no longer exist, it
	 * also deletes the related records in mu_apps_components_agents table
	 * through the fk and on cascade delete database option
	 * 
	 * @param mcAppID
	 * @param componentsToBeDeleted
	 */
	public void deleteNonExistingComponents(Long mcAppID, ArrayList<String> componentsToBeDeleted) {
		DBConnection DBCon = null;
		try {
			DBCon = new DBConnection();
			Statement stmt = DBCon.con.createStatement();
			StringJoiner joinerComponentNames = new StringJoiner("','", "'", "'");
			Iterator<String> iterator = componentsToBeDeleted.iterator();
			while (iterator.hasNext()) {
				joinerComponentNames.add(iterator.next());
			}

			// delete from mu_app_component table
			String sentence = "DELETE FROM mu_app_component WHERE (mu_app_component.app_id=" + mcAppID
					+ " and mu_app_component.component_name in (" + joinerComponentNames.toString() + "));";
			stmt.executeUpdate(sentence);

			stmt.close();
			DBCon.con.close();
			DBCon.con = null;
		} catch (SQLException excepcion) {
			DBCon.showException(excepcion);
		}

	}

	/**
	 * Returns the agents from mu_agents table
	 * 
	 * @return
	 */
	public Agents getAgents() {
		DBConnection DBCon = null;
		Agent obj = null;
		Agents agents = new Agents();

		try {
			DBCon = new DBConnection();
			Statement stmt = DBCon.con.createStatement();

			String str;
			str = " select mu_agents.id, mu_agents.name, mu_agents.description from mu_agents where mu_agents.avalilable=1;";

			ResultSet rs = stmt.executeQuery(str);

			while (rs.next()) {
				obj = new Agent();
				obj.setId(new Integer(rs.getInt("mu_agents.id")).toString());
				obj.setName(rs.getString("mu_agents.name"));
				obj.setDescription(rs.getString("mu_agents.description"));

				agents.addLinea(obj);
			}
			stmt.close();
			DBCon.con.close();
			DBCon.con = null;
		} catch (SQLException excepcion) {
			DBCon.showException(excepcion);
		}

		return agents;
	}

	/** Returns the components related to an application
	 * @param mcAppID
	 * @return
	 */
	public Components getComponentsByApplication(Long mcAppID) {
		DBConnection DBCon = null;
		Component obj = null;
		Components components = new Components();
		try {
			DBCon = new DBConnection();
			Statement stmt = DBCon.con.createStatement();

			String str;
			str = " select mu_app_component.app_id, mu_app_component.component_id, mu_app_component.component_name from mu_app_component where mu_app_component.app_id="
					+ mcAppID;

			ResultSet rs = stmt.executeQuery(str);

			while (rs.next()) {
				obj = new Component();
				obj.setAppId(new Integer(rs.getInt("mu_app_component.app_id")).toString());
				obj.setComponentId(new Integer(rs.getString("mu_app_component.component_id")).toString());
				obj.setComponentName(rs.getString("mu_app_component.component_name"));

				components.addLinea(obj);
			}
			stmt.close();
			DBCon.con.close();
			DBCon.con = null;
		} catch (SQLException excepcion) {
			DBCon.showException(excepcion);
		}

		return components;
	}

	/**
	 * Check if the table mu_apps_components_agents contains a record given
	 * appId, componentID and agentId.
	 * 
	 * @param appId
	 * @param componentId
	 * @param agentId
	 * @return
	 */
	public String checkIfAppAgentRelationExists(String appId, String componentId, String agentId) {
		String strResult = "false";
		DBConnection DBCon = null;
		int count = 0;
		Long mcAppID = new Long(appId);
		Integer mcComponentId = new Integer(componentId);
		Integer mcAgentID = new Integer(agentId);
		try {
			DBCon = new DBConnection();
			Statement stmt = DBCon.con.createStatement();

			String str;
			str = "select COUNT(*) AS count from musa_modeller.mu_apps_components_agents WHERE mu_apps_components_agents.app_id="
					+ mcAppID + " and mu_apps_components_agents.agent_id=" + mcAgentID;

			ResultSet rs = stmt.executeQuery(str);

			if (rs.next()) {
				count = rs.getInt("count");
				if (count > 0)
					strResult = "true";
			}

			stmt.close();
			DBCon.con.close();
			DBCon.con = null;
		} catch (SQLException excepcion) {
			DBCon.showException(excepcion);
		}

		return strResult;
	} // fin

	/** Updates camel and xmlCamel values in the database
	 * @param mcAppID
	 * @param strUpdatedCamelFile
	 * @param strUpdatedCamelFileXMI
	 * @return
	 */
	public boolean updateCamelAndXMIofMCApp(Long mcAppID, String strUpdatedCamelFile, String strUpdatedCamelFileXMI) {
		DBConnection DBCon = null;
		camel = strUpdatedCamelFile;
		camelXMI = strUpdatedCamelFileXMI;
		try {
			removeWhiteSpaces();
			DBCon = new DBConnection();
			Statement stmt = DBCon.con.createStatement();

			String str;			
			str = "UPDATE mu_mcapp SET Camel='" + camel + "', CamelXMI='" + camelXMI + "'" + " WHERE mu_mcapp.id= "
					+ mcAppID;

			stmt.executeUpdate(str);
			stmt.close();
			DBCon.con.close();
			DBCon.con = null;
		} catch (Exception excepcion) {
			if (excepcion instanceof SQLException)
				FuncGenerales.showException(excepcion);			
			else
				FuncGenerales.showException(excepcion);
			return false;
		}
		return true;
	}

	/** Returns a SecurityCapability object given an agent ID
	 * @param agentId
	 * @return
	 */
	public SecurityCapability getSecurityCapabilityByAgentId(String agentId) {
		DBConnection DBCon = null;
		SecurityCapability securityCapability = null;
		int intAgentId = new Integer(agentId).intValue();

		try {
			DBCon = new DBConnection();
			Statement stmt = DBCon.con.createStatement();

			String str;
			str = " select musa_modeller.mu_security_capabilities.id,musa_modeller.mu_security_capabilities.name,musa_modeller.mu_security_capabilities.value from musa_modeller.mu_security_capabilities, musa_modeller.mu_agents where mu_security_capabilities.id = mu_agents.security_capability_id and mu_agents.id="
					+ intAgentId;

			ResultSet rs = stmt.executeQuery(str);

			while (rs.next()) {
				securityCapability = new SecurityCapability();
				securityCapability.setId(rs.getInt("mu_security_capabilities.id"));

				securityCapability.setName(rs.getString("mu_security_capabilities.name"));
				securityCapability.setValue(rs.getString("mu_security_capabilities.value"));
			}
			stmt.close();
			DBCon.con.close();
			DBCon.con = null;
		} catch (SQLException excepcion) {
			DBCon.showException(excepcion);
		}

		return securityCapability;

	}

	/** Returns the security agent model given its id
	 * @param agentId
	 * @return
	 */
	public String getSecurityAgentCamelModel(String agentId) {
		DBConnection DBCon = null;
		String securityAgentCamelModel = "";
		int intAgentId = new Integer(agentId).intValue();

		try {
			DBCon = new DBConnection();
			Statement stmt = DBCon.con.createStatement();

			String str;
			str = " select musa_modeller.mu_agents.camel_model from musa_modeller.mu_agents where musa_modeller.mu_agents.id="
					+ intAgentId;

			ResultSet rs = stmt.executeQuery(str);

			while (rs.next()) {
				securityAgentCamelModel = rs.getString("mu_agents.camel_model");
			}
			stmt.close();
			DBCon.con.close();
			DBCon.con = null;
		} catch (SQLException excepcion) {
			DBCon.showException(excepcion);
		}

		return securityAgentCamelModel;
	}

	/**Establish a relation between an agent an an application component in the database	
	 * 
	 * @param appId
	 * @param componentId
	 * @param agentId
	 * @return
	 */
	public String setApplicationComponentAgentRelation(String appId, String componentId,
			String agentId) {
		String strResult = "false";
		DBConnection DBCon = null;		
		Long mcAppID = new Long(appId);
		Integer mcComponentId = new Integer(componentId);
		Integer mcAgentID = new Integer(agentId);
		try {
			DBCon = new DBConnection();
			Statement stmt = DBCon.con.createStatement();

			String sentence = "";
			// a new record is inserted
			sentence = "INSERT INTO musa_modeller.mu_apps_components_agents (`app_id`, `component_id`, `agent_id`) VALUES ("
					+ mcAppID + ", " + mcComponentId + ", " + mcAgentID + ")";
			stmt.executeUpdate(sentence);
			
			stmt.close();
			DBCon.con.close();
			DBCon.con = null;
		} catch (SQLException excepcion) {
			DBCon.showException(excepcion);
		}

		return strResult;
	}
}
