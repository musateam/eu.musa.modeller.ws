package eu.musa.modeller.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Component")
// JAX-RS supports an automatic mapping from JAXB annotated class to XML and JSON
public class Component {
	private String appId;
    private String componentId;
    private String componentName;
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getComponentId() {
		return componentId;
	}
	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}
	public String getComponentName() {
		return componentName;
	}
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}
    
	
    
    
    
}