package eu.musa.modeller.model;


import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="ValidationErrors")
public class ValidationErrors {
    private ArrayList<ValidationError> list = new ArrayList<ValidationError>();

    
    @XmlElement(name="ValidationError")
    public ArrayList<ValidationError> getList() {
        return list;
    }

    public void setList(ArrayList<ValidationError> list) {
        this.list = list;
    }
    
    public void addLinea(ValidationError l) {
        this.list.add(l);
    }
}
