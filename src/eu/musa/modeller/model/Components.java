package eu.musa.modeller.model;


import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="Components")
public class Components {
    private ArrayList<Component> list = new ArrayList<Component>();

    
    @XmlElement(name="Component")
    public ArrayList<Component> getList() {
        return list;
    }

    public void setList(ArrayList<Component> list) {
        this.list = list;
    }
    
    public void addLinea(Component l) {
        this.list.add(l);
    }
}
