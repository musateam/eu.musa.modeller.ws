package eu.musa.modeller.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="SecurityCapability")
// JAX-RS supports an automatic mapping from JAXB annotated class to XML and JSON
public class SecurityCapability {
	private int id;
    private String name;
    private String value;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
    
	
    
    
}