package eu.musa.modeller.model;


import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="CamelFiles")
public class CamelFiles {
    private ArrayList<CamelFile> list = new ArrayList<CamelFile>();

    
    @XmlElement(name="CamelFile")
    public ArrayList<CamelFile> getList() {
        return list;
    }

    public void setList(ArrayList<CamelFile> list) {
        this.list = list;
    }
    
    public void addLinea(CamelFile l) {
        this.list.add(l);
    }
}
