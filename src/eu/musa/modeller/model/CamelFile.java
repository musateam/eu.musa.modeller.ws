package eu.musa.modeller.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="CamelFile")
// JAX-RS supports an automatic mapping from JAXB annotated class to XML and JSON
public class CamelFile {
	private String Id;
    private String MCApplication;
    private String MCAVersion;	    
    private String Camel;
    
    public void setId(String id) {
		Id = id;
	}
    @XmlElement(name="id")
    public String getId() {
		return Id;
	}
		
    @XmlElement(name="mcapplication")
    public String getMCApplication() {
        return MCApplication;
    }
 
    public void setMCApplication(String value) {
        this.MCApplication = value;
    }

    @XmlElement(name="mcaversion")
    public String getMCAVersion() {
        return MCAVersion;
    }
 
    public void setMCAVersion(String value) {
        this.MCAVersion = value;
    }
    
    @XmlElement(name="camel")    
    public String getCamel() {
        return Camel;
    }
 
    public void setCamel(String value) {
        this.Camel = value;
    }
}