package eu.musa.modeller.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="ValidationError")
// JAX-RS supports an automatic mapping from JAXB annotated class to XML and JSON
public class ValidationError {
	private String id;
	private String line;	
    private String type;
    private String description;
    
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLine() {
		return line;
	}
	public void setLine(String line) {
		this.line = line;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
    
    
    
}