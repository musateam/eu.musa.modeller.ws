package eu.musa.modeller.model;


import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="Agents")
public class Agents {
    private ArrayList<Agent> list = new ArrayList<Agent>();

    
    @XmlElement(name="Agent")
    public ArrayList<Agent> getList() {
        return list;
    }

    public void setList(ArrayList<Agent> list) {
        this.list = list;
    }
    
    public void addLinea(Agent l) {
        this.list.add(l);
    }
}
