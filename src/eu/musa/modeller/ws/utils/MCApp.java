package eu.musa.modeller.ws.utils;

import java.util.Date;

public class MCApp {
	    private Long id;	    
		private String MCApplication;
	    private String MCAVersion;	    
	    private String Camel;	      
	    private Date date;
	    private String CamelXMI;	    

	    public MCApp() {}
	 	    
	    public Long getId() {
			return id;
		}

	    public void setId(Long id) {
			this.id = id;
		}   		
		public Date getDate() {
	        return date;
	    }
	 
	    public void setDate(Date date) {
	        this.date = date;
	    }
	    

		public String getMCApplication() {
	        return MCApplication;
	    }
	 
	    public void setMCApplication(String value) {
	        this.MCApplication = value;
	    }

	    public String getMCAVersion() {
	        return MCAVersion;
	    }
	 
	    public void setMCAVersion(String value) {
	        this.MCAVersion = value;
	    }
	    
	    
	    public String getCamel() {
	        return Camel;
	    }

	    public String getCamelXMI() {
	        return CamelXMI;
	    }
	    
	    public void setCamel(String value) {
	        this.Camel = value;
	    }
	    
	    public void setCamelXMI(String value) {
	        this.CamelXMI = value;
	    }	    
}
