package eu.musa.modeller.ws.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import eu.musa.modeller.model.Agents;
import eu.musa.modeller.model.CamelFile;
import eu.musa.modeller.model.CamelFiles;
import eu.musa.modeller.model.Components;
import eu.musa.modeller.model.ManageMCAppDB;
import eu.musa.modeller.model.SecurityCapability; 
 
//import eu.musa.modeller.utils.MCApp;

public class ManageMCApp {
		
	PrintWriter out = null;
	
	/** Gets a list of CamelFiles objetcs
	 * @return
	 */
	public CamelFiles getMCApps(){			
		CamelFiles res = new CamelFiles();
		ManageMCAppDB db = new ManageMCAppDB();
		Vector vec = db.getMCApps();				

	    for (int i=0; i<vec.size();i++){
	    	MCApp mcapp = (MCApp)vec.elementAt(i);
	    	
		    CamelFile clone = new CamelFile();		    		    		    
		    clone.setId(mcapp.getId()+".camel");
		    //GEV It is neccesary to include the extension because otherwhise it happens an xtext error 
		    clone.setMCApplication(mcapp.getMCApplication());		   
		    clone.setMCAVersion(mcapp.getMCAVersion());
		    clone.setCamel(mcapp.getCamel());		    
		    res.addLinea(clone);
		}
		
		return res;
	}
	
	/**
	 * Returns the agents from mu_agents table
	 * 
	 * @return
	 */
	public Agents getAgents(){					
		ManageMCAppDB db = new ManageMCAppDB();
		Agents res =  db.getAgents();					    		
		return res;
	}
	
	/** Returns the components related to an application
	 * @param mcAppID
	 * @return
	 */
	public Components getComponents(Long lngAppId) {
		ManageMCAppDB db = new ManageMCAppDB();
		Components res =  db.getComponentsByApplication(lngAppId);					    		
		return res;
	}
	
	
	/** Returns the camel file of an application given its id
	 * @return
	 */
	public String getCamel(Long mcAppID){		
		String strCamel = "";
		ManageMCAppDB db = new ManageMCAppDB();
		strCamel = db.getAppCamel(mcAppID);			
		return strCamel;
	}
	
	/** Returns the camel file in xml format of an application given its id
	 * @return
	 */
	public String getCamelXML(Long mcAppID){		
		String strCamel = "";
		ManageMCAppDB db = new ManageMCAppDB();
		strCamel = db.getAppCamelXML(mcAppID);			
		return strCamel;
	}

	/** Returns the validation report of an application given its id
	 * @return
	 */
	public String getValidationReport(Long mcAppID){		
		String strReport = "";
		ManageMCAppDB db = new ManageMCAppDB();
		strReport = db.getAppValidationReport(mcAppID);			
		return strReport;
	}
	
	/** Returns the number of errors in the validation report of an application given its id
	 * @return
	 */
	public Integer getErrorsValidationReport(Long mcAppID){		
		Integer iReport = 0;
		ManageMCAppDB db = new ManageMCAppDB();
		iReport = db.getAppErrorsValidationReport(mcAppID);			
		return iReport;
	}
	
	/** Returns true if the app exists in the database and false if not
	 * @return
	 */
	public String checkIfAppExistsInDB(Long mcAppID){		
		String response = "";
		ManageMCAppDB db = new ManageMCAppDB();
		response = db.checkIfAppExists(mcAppID);		
		return response;
	}		
	
	/** Returns a map containing the relation of all the components used by an application
	 * @return
	 */
	public HashMap<Object, Object> getComponentsIdsByApplication(Long mcAppID) {		
		HashMap<Object, Object> componentMap = new HashMap<>();
		ManageMCAppDB db = new ManageMCAppDB();
		componentMap = db.getComponentsIdsByApplication(mcAppID);		
		return componentMap;
	}
	
	/** Inserts new component codes to mu_app_component table
	 * @return
	 */
	public void insertMissingComponentCodes(Long mcAppID,HashMap<Object, Object> componentsToBeInserted) {		
		ManageMCAppDB db = new ManageMCAppDB();
		db.insertMissingComponentCodes(mcAppID,componentsToBeInserted);				
	}

	/** Deletes components that have been eliminated through Musa Modeller from both mu_app_component and mu_apps_components_agents tables
	 * @param mcAppID
	 * @param componentsToBeDeleted
	 */
	public void deleteNonExistingComponents(Long mcAppID, ArrayList<String> componentsToBeDeleted) {
		ManageMCAppDB db = new ManageMCAppDB();
		db.deleteNonExistingComponents(mcAppID,componentsToBeDeleted);	
		
	}

	/** Check if the table mu_apps_components_agents contains a record given appId, componentID and agentId. 		 
	 * @param appId
	 * @param componentId
	 * @param agentId
	 * @return
	 */
	public String checkIfAppAgentRelationExists(String appId, String agentId, String componentId) {		
		//**1 if there is not a record in mu_apps_agents for this app_id and agent_id creates a new security capability named CAP_APPID_COMPONENTID in the camel file
		String response = "";
		ManageMCAppDB db = new ManageMCAppDB();
		response = db.checkIfAppAgentRelationExists(appId,componentId,agentId);		
		return response;		
		
	}

	/** Updates camel and xmlCamel values in the database
	 * @param mcAppID
	 * @param strUpdatedCamelFile
	 * @param strUpdatedCamelFileXMI
	 * @return
	 */
	public void updateCamelAndXMIofMCApp(String appId, String strUpdatedCamelFile, String strUpdatedCamelFileXMI) {
		ManageMCAppDB db = new ManageMCAppDB();
		db.updateCamelAndXMIofMCApp(new Long(appId), strUpdatedCamelFile,strUpdatedCamelFileXMI);
		
	}

	/** Returns a SecurityCapability object given an agent ID
	 * @param agentId
	 * @return
	 */
	public SecurityCapability getSecurityCapabilityByAgentId(String agentId) {
		SecurityCapability securityCapability=null;
		ManageMCAppDB db = new ManageMCAppDB();
		securityCapability=db.getSecurityCapabilityByAgentId(agentId);
		return securityCapability;
	}

	/** Updates camel file in the file system
	 * @param strUpdatedCamelModel
	 * @param strFilePath
	 */
	public void updateCamelFileInFilesystem(String strUpdatedCamelModel, String strFilePath) {		
		boolean appendFile = false;
		File fnew=new File(strFilePath);		
		FileWriter f2;
		try {		    
		    f2 = new FileWriter(fnew,appendFile); // important part
		    f2.write(strUpdatedCamelModel);		  
		    appendFile = true;

		    f2.close();
		 } catch (IOException e) {
		       // TODO Auto-generated catch block
		       e.printStackTrace();
		 }           
		
	}

	/** Returns the security agent model given its id
	 * @param agentId
	 * @return
	 */
	public String getSecurityAgentCamelModel(String agentId) {
		String securityAgentCamelModel="";
		ManageMCAppDB db = new ManageMCAppDB();
		securityAgentCamelModel=db.getSecurityAgentCamelModel(agentId);
		return securityAgentCamelModel;
	}

	/**Establish a relation between an agent an an application component in the database 	 
	 * 
	 * @param appId
	 * @param componentId
	 * @param agentId
	 * @return
	 */
	public String setApplicationComponentAgentRelation(String appId, String agentId, String componentId) {
		String response = "";
		ManageMCAppDB db = new ManageMCAppDB();
		response = db.setApplicationComponentAgentRelation(appId,componentId,agentId);		
		return response;	
	}
	
	
}