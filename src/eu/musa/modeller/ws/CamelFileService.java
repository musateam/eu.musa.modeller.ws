package eu.musa.modeller.ws;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.camel_dsl.dsl.CamelDslStandaloneSetup;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.google.inject.Injector;

import eu.musa.modeller.model.Agents;
import eu.musa.modeller.model.CamelFiles;
import eu.musa.modeller.model.Components;
import eu.musa.modeller.model.ManageMCAppDB;
import eu.musa.modeller.model.SecurityCapability;
import eu.musa.modeller.model.ValidationError;
import eu.musa.modeller.model.ValidationErrors;
import eu.musa.modeller.ws.utils.ManageMCApp;
import eu.musa.modeller.ws.utils.RESTClient;

import java.util.logging.*;

/**
 * @author 106800
 *
 */

@Path("/camelfilews")
public class CamelFileService {
	private final String MUSAAGENTSLINUXPATH = "/home/MUSA/MODELLER/db-files";
	private final String MUSAAGENTSWINDOWSPATH = "D:/MUSA/db-files";
	private final String MUSASESSIONSERVICEURL = "http://framework.musa-project.eu/session/";
	private final String GETCAMELMODELDIRAPPENDIX = "camelfilews/getCamel/APP_ID/CAMEL";
	private final String GETMACMMODELDIRAPPENDIX = "camelfilews/getCamel/APP_ID/MACM";
	private final String LOCAL_URL_BASE = "http://localhost:8080/eu.musa.modeller.ws/webresources/";
	private final String DEV_URL_BASE = "http://modeller-dev.musa-project.eu:8080/eu.musa.modeller.ws/webresources/";
	private final String PROD_URL_BASE = "http://framework.musa-project.eu/modeller/eu.musa.modeller.ws/webresources/";

	/**
	 * For testing purposes
	 * 
	 * @return
	 */
	@GET
	@Path("prueba")
	@Produces("application/json")
	public String prueba() {
		return "Prueba de servicio correcta!!";

	}

	/**
	 * This operation is called if apps list is requested
	 * 
	 * @return
	 */
	@GET
	@Path("/getMCApps")
	@Produces({ MediaType.APPLICATION_JSON })
	public CamelFiles getMCApps() {
		ManageMCApp base = new ManageMCApp();
		CamelFiles res = base.getMCApps();
		return res;
	}

	/**
	 * Operation called if agents list is requested
	 * 
	 * @return
	 */
	@GET
	@Path("/getAgents")
	@Produces({ MediaType.APPLICATION_JSON })
	public Agents getAgents() {
		ManageMCApp base = new ManageMCApp();
		Agents res = base.getAgents();
		return res;
	}

	/**
	 * This operation is called when an agent is added to an application
	 * 
	 * @param appId
	 * @param appName
	 * @param agentId
	 * @param agentName
	 * @param componentId
	 * @param componentName
	 * @return
	 */
	@PUT
	@Path("/putAgent/{appId}/{appName}/{agentId}/{agentName}/{componentId}/{componentName}")
	@Produces("text/plain;charset=UTF-8")
	public String putAgent(@PathParam("appId") String appId, @PathParam("appName") String appName,
			@PathParam("agentId") String agentId, @PathParam("agentName") String agentName,
			@PathParam("componentId") String componentId, @PathParam("componentName") String componentName) {
		ManageMCApp base = new ManageMCApp();
		String strUpdatedCamelModel = "";

		String returnMessage = "";

		// if security capability for this agent already exists it is not
		// included
		strUpdatedCamelModel = updateCamelModelWithAgentData(appId, agentId, componentId, agentName, componentName);

		returnMessage = strUpdatedCamelModel;

		return returnMessage;
	}

	/**
	 * This method is called when a model has been modified in order to check
	 * and update if neccesary the mu_app_component and
	 * mu_apps_components_agents tables. The component names arrive separated by
	 * commas. It can delete components from mu_app_component in case they exist
	 * in the database but not in the model or it can insert components in case
	 * they exist in the model but not in the database
	 * 
	 * @param appId
	 * @param componentValues
	 * @return
	 */
	@PUT
	@Path("/upDateComponentsPerApplication/{appId}/{componentValues}")
	@Produces("text/plain")
	public String upDateComponentsPerApplication(@PathParam("appId") String appId,
			@PathParam("componentValues") String componentValues) {
		String res = "true";
		ManageMCApp base = new ManageMCApp();
		Long mcAppID = new Long(appId);
		String xmiModel = getCamel(appId, "XML");

		ArrayList<String> componentsToBeDeleted = new ArrayList<String>();
		HashMap<Object, Object> componentsToBeInserted = new HashMap<Object, Object>();

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document doc = null;
		String content = "";

		String componentName = "";
		String componentType = "";
		String strComponentTypeXPath = "";
		String strComponentType = "";
		String componentNamesInDB = "";
		// "camelDsl:CamelPlusModel" element name gives problems
		content = xmiModel.replace("camelDsl:CamelPlusModel", "camelDsl");

		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			doc = builder.parse(new InputSource(new StringReader(content)));
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		XPathExpression expr = null;
		try {
			expr = xpath.compile("camelDsl/deploymentModels/internalComponents");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		NodeList internalComponentsNodeList = null;

		try {
			internalComponentsNodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		for (int i = 0; i < internalComponentsNodeList.getLength(); i++) {
			componentName = internalComponentsNodeList.item(i).getAttributes().getNamedItem("name").getTextContent();
			strComponentType = "";
			if ((internalComponentsNodeList.item(i).getAttributes().getNamedItem("type") != null)
					&& (internalComponentsNodeList.item(i).getAttributes().getNamedItem("type")
							.getTextContent() != "")) {
				componentType = internalComponentsNodeList.item(i).getAttributes().getNamedItem("type")
						.getTextContent();
				strComponentTypeXPath = getXPathExpression(componentType);
				try {
					expr = xpath.compile(strComponentTypeXPath);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Node musaComponentTypeNode = null;
				try {
					musaComponentTypeNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				strComponentType = musaComponentTypeNode.getAttributes().getNamedItem("name").getTextContent();

			}

			// we insert only those components whose type != AGENT*
			if (!strComponentType.startsWith("AGENT.")) {
				componentNamesInDB += componentName;
				if ((componentValues.equalsIgnoreCase("empty")) || (!componentValues.contains(componentName))) {
					// it is given null value to componentId in order to get the
					// maximun existing value when executing the query
					componentsToBeInserted.put(componentName, null);
				}
			}

		}
		// if the number of internal components in the model is bigger, the
		// insertion is executed
		if (componentsToBeInserted.size() > 0) {
			base.insertMissingComponentCodes(mcAppID, componentsToBeInserted);
		}

		if (componentValues.equalsIgnoreCase("empty"))
			return (res);

		StringTokenizer strTokenizerComponentValues = new StringTokenizer(componentValues, ",");
		while (strTokenizerComponentValues.hasMoreElements()) {
			componentName = (String) strTokenizerComponentValues.nextElement();
			// if the component is not included in the model it will be deleted
			// from the database
			if (!componentNamesInDB.contains(componentName)) {
				componentsToBeDeleted.add(componentName);
			}
		}
		if (componentsToBeDeleted.size() > 0) {
			// deletes the components that are in combo but not in the model, so
			// next time the combo will be load with the existing components
			base.deleteNonExistingComponents(mcAppID, componentsToBeDeleted);
		}

		// check if combo size is the same as number of internal components in
		// the model

		return (res);

	}

	/**
	 * This method is called when a model has been modified and it is necessary
	 * to update the timestamp of this update and the urls so it is possible to
	 * get the CAMEL and MACM models
	 * 
	 * @param appId
	 * @param env
	 * @param authorization
	 * @return
	 */
	@PUT
	@Path("/updateTimestampAndURLS/{appId}/{env}")
	@Produces("text/plain")
	public String updateTimestampAndURLS(@PathParam("appId") String appId, @PathParam("env") String env,
			@HeaderParam("Authorization") String authorization) {
		String res = "true";
		String urlBase = "";
		if ((env != null) && (env.equalsIgnoreCase("LOCAL")))
			urlBase = LOCAL_URL_BASE;
		else if ((env != null) && (env.equalsIgnoreCase("DEV")))
			urlBase = DEV_URL_BASE;
		else if ((env != null) && (env.equalsIgnoreCase("PROD")))
			urlBase = PROD_URL_BASE;

		String getModelUrl = urlBase + GETCAMELMODELDIRAPPENDIX.replace("APP_ID", appId);
		String getMACMUrl = urlBase + GETMACMMODELDIRAPPENDIX.replace("APP_ID", appId);

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", authorization);

		Map<String, String> mapJObject = new HashMap<String, String>();

		String json = null;
		String strSessionServiceResponse = "";
		try {
			json = RESTClient.REQUEST(MUSASESSIONSERVICEURL + appId, "get", headers, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (json == null) {
			return "false";
		}
		JSONObject jObject = null;
		try {

			jObject = new JSONObject(json);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			res = "false";
			e.printStackTrace();
		}

		try {
			jObject.put("ModelTS", Instant.now().toString());
			jObject.put("getModelUrl", getModelUrl);
			jObject.put("getMACMUrl", getMACMUrl);
			// UTC format -> example value 2017-09-21T13:09:34.317Z
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			res = "false";
			e.printStackTrace();
		}

		mapJObject.put("", jObject.toString());

		try {
			strSessionServiceResponse = RESTClient.REQUEST(MUSASESSIONSERVICEURL + appId, "put", headers, mapJObject);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			res = "false";
			e.printStackTrace();
		}
		return (res);

	}

	/**
	 * The camel model and the xmi model are modified. If there is not a record
	 * in mu_apps_agents for this app_id and agent_id it creates a new security
	 * capability in the camel file and inserts a new record that includes the
	 * camel model of the agent like other internal component
	 * 
	 * @param appId
	 * @param agentId
	 * @param componentId
	 * @param agentName
	 * @param componentName
	 * @param agentAlreadyAssignedToApp
	 * @return
	 */
	private String updateCamelModelWithAgentData(String appId, String agentId, String componentId, String agentName,
			String componentName) {
		String strUpdatedCamel = "";
		SecurityCapability securityCapability = null;
		ManageMCApp base = new ManageMCApp();
		Long lngAppId = new Long(appId);
		String strCamel = base.getCamel(lngAppId);
		String strCamelWithNewSecurityCapability = strCamel;
		securityCapability = base.getSecurityCapabilityByAgentId(agentId);
		boolean agentAlreadyAssignedToApp = checkIfAgentAlreadyAssignedToApp(strCamel, securityCapability);
		if (agentAlreadyAssignedToApp == false) {
			strCamelWithNewSecurityCapability = setSecurityCapability(strCamel, securityCapability.getValue());
		}
		String strSecurityAgentCamelModel = base.getSecurityAgentCamelModel(agentId);
		String strSecurityModelName = getSecurityModelName(strCamelWithNewSecurityCapability);
		String strCamelWithSecurityAgentCamelModel = setSecurityAgentCamelModel(strCamelWithNewSecurityCapability,
				strSecurityAgentCamelModel, agentName, componentName, strSecurityModelName);// yes
		String strCamelWithHostingForSecurityAgent = setHostingForSecurityAgent(strCamelWithSecurityAgentCamelModel,
				strSecurityAgentCamelModel, agentName, componentName, strSecurityModelName);// yes
		String strCamelWithCapabilityMatchForSecurityAgent = setCapabilityMatchForSecurityAgent(
				strCamelWithHostingForSecurityAgent, strSecurityAgentCamelModel, agentName, componentName,
				strSecurityModelName);// yes
		// GEV commented due to some problems when parsing the camel file and
		// transform to xmi
		String strCamelWithRequiredSecurityCapability = setRequiredSecurityCapability(
				strCamelWithCapabilityMatchForSecurityAgent, strSecurityAgentCamelModel, agentName, componentName,
				strSecurityModelName);// yes
		// String strCamelWithRequiredSecurityCapability =
		// strCamelWithCapabilityMatchForSecurityAgent;
		strUpdatedCamel = strCamelWithRequiredSecurityCapability;
		return strUpdatedCamel;
	}

	/**
	 * Checks if an agent has been already assigned to an app
	 * 
	 * @param strCamel
	 * @param securityCapability
	 * @return
	 */
	private boolean checkIfAgentAlreadyAssignedToApp(String strCamel, SecurityCapability securityCapability) {
		if (!strCamel.contains(securityCapability.getName()))
			return false;
		else
			return true;
	}

	/**
	 * Returns the name of the security model from the camel model
	 * 
	 * @param strCamel
	 * @return
	 */
	private String getSecurityModelName(String strCamel) {
		String strSecurityModelName = "";
		int matchedIndex = getMatchedEndIndex(strCamel, "security model(\\s*)(\\S+)(\\s*)(\\{)");
		String strAux = strCamel.substring(strCamel.indexOf("security model"), matchedIndex - 1);
		strAux = strAux.replaceAll("security model", "");
		strSecurityModelName = strAux.replaceAll("\\s", "");
		return strSecurityModelName;
	}

	/**
	 * Inserts the model of the security capability in the camel file
	 * 
	 * @param strCamelWithNewSecurityCapability
	 * @param strSecurityAgentCamelModel
	 * @return
	 */
	private String setSecurityAgentCamelModel(String strCamelWithNewSecurityCapability,
			String strSecurityAgentCamelModel, String agentName, String componentName, String securityModelName) {
		String strCamelWithSecurityAgentCamelModel = "";

		// if there is not an internal component, it is included just after the
		// deployment model declaration
		String aux = strSecurityAgentCamelModel.replaceAll("AGENT_INTERNAL_COMPONENT", "MUSA" + agentName + "For" + componentName);
		aux = aux.replaceAll("COMPONENT_NAME", componentName);
		String dynamicCamel = aux.replaceAll("SECURITY_MODEL_NAME", securityModelName);
		/*
		 * int indexOf = strCamelWithNewSecurityCapability.lastIndexOf("}");
		 * 
		 * strCamelWithSecurityAgentCamelModel =
		 * strCamelWithNewSecurityCapability.substring(0, indexOf - 1) + "\n" +
		 * dynamicCamel + "\n}";
		 */

		int indexOf = strCamelWithNewSecurityCapability.indexOf("internal component");
		int matchedIndex = 0;
		int insertionPosition = 0;
		// if there is an internal component already included in the model it is
		// inserted just before it
		if (indexOf != -1) {
			matchedIndex = getMatchedEndIndex(strCamelWithNewSecurityCapability, "internal component");
			insertionPosition = matchedIndex - "internal component".length();
			strCamelWithSecurityAgentCamelModel = strCamelWithNewSecurityCapability.substring(0, insertionPosition)
					+ dynamicCamel + strCamelWithNewSecurityCapability.substring(insertionPosition);
		}
		// if not, it is included just after the deployment model declaration
		else {
			matchedIndex = getMatchedEndIndex(strCamelWithNewSecurityCapability,
					"deployment model(\\s+)(\\S+)(\\s+)(\\{)");
			strCamelWithSecurityAgentCamelModel = strCamelWithNewSecurityCapability.substring(0, matchedIndex)
					+ dynamicCamel + strCamelWithNewSecurityCapability.substring(matchedIndex + 1);
		}

		return strCamelWithSecurityAgentCamelModel;
	}

	/**
	 * Inserts the hosting related part at the end of the model
	 * 
	 * @param strCamelWithSecurityAgentCamelModel
	 * @param strSecurityAgentCamelModel
	 * @param agentName
	 * @param componentName
	 * @param strSecurityModelName
	 * @return
	 */
	private String setHostingForSecurityAgent(String strCamelWithSecurityAgentCamelModel,
			String strSecurityAgentCamelModel, String agentName, String componentName, String strSecurityModelName) {

		String strCamelWithHostingForSecurityAgent = "";
		String toVMName = "";
		String toHostName = "";
		
		Node vmNode = getHostingToVMNode (strCamelWithSecurityAgentCamelModel, componentName);

		if (vmNode!=null){
			if ((vmNode!=null)&&(vmNode.getAttributes()!=null)&&(vmNode.getAttributes().getNamedItem("name")!=null)){
				toVMName = vmNode.getAttributes().getNamedItem("name").getTextContent();
			}
			NodeList childNodes = vmNode.getChildNodes();		
			if ((childNodes!=null)&&(childNodes.getLength()>0)){
				Node childNode = childNodes.item(1);
				if (childNode.getNodeName().equalsIgnoreCase("providedHosts")){
					if ((childNode.getAttributes()!=null)&&(childNode.getAttributes().getNamedItem("name")!=null)){
						toHostName=childNode.getAttributes().getNamedItem("name").getTextContent();
					}
					
				}
							
			}
		}				
				
		/*
		 * hosting MUSAACAgentForTSMEngineHosting { from
		 * MUSAACAgentForTSMEngine.MUSAAgentACHostReq to
		 * CPUIntensiveUbuntuFinlandVM.CPUIntensiveUbuntuFinlandHost }
		 */
		String agentNameComponentName = "MUSA" + agentName + componentName;

		String strHostingInfoDynamicallyGenerated = "hosting " + agentName + "For" + componentName + "Hosting {\n"
				+ "\t\tfrom MUSA" + agentName + "For" + componentName + "." + agentName + "HostReq  to " + toVMName
				+ "." + toHostName+"\n\t\t}\n\t\t";
		

		int matchedIndex = getMatchedStartIndex(strCamelWithSecurityAgentCamelModel, "hosting(\\s+)(\\S+)(\\s+)(\\{)");
		// int insertionPosition =0;
		// if there is an internal component already included in the model it is
		// inserted just before it
		if (matchedIndex != 0) {
			strCamelWithHostingForSecurityAgent = strCamelWithSecurityAgentCamelModel.substring(0, matchedIndex)
					+ strHostingInfoDynamicallyGenerated + strCamelWithSecurityAgentCamelModel.substring(matchedIndex);
		}
		// if not, it is included just after the deployment model declaration
		else {
		}

		return strCamelWithHostingForSecurityAgent;
	}

	/**
	 * Returns the vm Node in a hosting element given the component name
	 * 
	 * @param doc
	 * @param componentName
	 * @return
	 */
	private Node getHostingToVMNode(String strCamelModel, String componentName) {

		String strXMIModel = getCamelInXMIFormat(strCamelModel);
		
		Node vmNode=null;

		strXMIModel = strXMIModel.replace("camelDsl:CamelPlusModel", "camelDsl");

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document doc = null;

		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			doc = builder.parse(new InputSource(new StringReader(strXMIModel)));
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// xpath inizialitation
		// firs get the requredhost of the internal component in order to get
		// the hosting node
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		XPathExpression expr = null;

		try {
			expr = xpath.compile(
					"camelDsl/deploymentModels/internalComponents[@name='" + componentName + "']/requiredHost/@name");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String requiredHostName = null;
		try {
			requiredHostName = (String) expr.evaluate(doc, XPathConstants.STRING);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			expr = xpath.compile("camelDsl/deploymentModels/hostings");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		NodeList hostingsNodeList = null;
		try {
			hostingsNodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int lenght = hostingsNodeList.getLength();
		int index = 0;
		String providedHost="";
		String requiredHost="";
		while (index < lenght) {			
			Node hostingNode = hostingsNodeList.item(index);
			if (hostingNode.getAttributes().getNamedItem("requiredHost") != null) {
				requiredHost = hostingNode.getAttributes().getNamedItem("requiredHost").getTextContent();
				String requiredHostXPathExpression = getXPathExpression(requiredHost);

				try {
					expr = xpath.compile(requiredHostXPathExpression);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Node requiredHostNode = null;
				try {
					requiredHostNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
				} catch (XPathExpressionException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if ((requiredHostNode!=null)&&(requiredHostNode.getAttributes().getNamedItem("name").getTextContent()!=null)){
					if (requiredHostNode.getAttributes().getNamedItem("name").getTextContent().equalsIgnoreCase(requiredHostName)){
						if (hostingNode.getAttributes().getNamedItem("providedHost") != null) {
							providedHost=hostingNode.getAttributes().getNamedItem("providedHost").getTextContent();
							String providedHostXPathExpression = getXPathExpression(providedHost);

							try {
								expr = xpath.compile(providedHostXPathExpression);
							} catch (XPathExpressionException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							Node providedHostNode = null;
							try {
								providedHostNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
								if (providedHostNode!=null){
									vmNode=providedHostNode.getParentNode();
									return vmNode;
								}
							} catch (XPathExpressionException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
					}			
				}//end if
			}//end if
			index++;
		}//end while										
		return vmNode;
	}

	/**
	 * @param strCamelWithSecurityAgentCamelModel
	 * @param componentName
	 * @return
	 */
	private String getHostingToVMNodeFromModel(String strCamelModel, String componentName) {
		String strXMIModel = getCamelInXMIFormat(strCamelModel);
		
		String toHostName = "";

		strXMIModel = strXMIModel.replace("camelDsl:CamelPlusModel", "camelDsl");

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document doc = null;

		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			doc = builder.parse(new InputSource(new StringReader(strXMIModel)));
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// xpath inizialitation
		// firs get the requredhost of the internal component in order to get
		// the hosting node
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		XPathExpression expr = null;

		try {
			expr = xpath.compile(
					"camelDsl/deploymentModels/internalComponents[@name='" + componentName + "']/requiredHost/@name");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String requiredHostName = null;
		try {
			requiredHostName = (String) expr.evaluate(doc, XPathConstants.STRING);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			expr = xpath.compile("camelDsl/deploymentModels/hostings");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		NodeList hostingsNodeList = null;
		try {
			hostingsNodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int lenght = hostingsNodeList.getLength();
		int index = 0;
		String providedHost="";
		String requiredHost="";
		while (index < lenght) {			
			Node hostingNode = hostingsNodeList.item(index);
			if (hostingNode.getAttributes().getNamedItem("requiredHost") != null) {
				requiredHost = hostingNode.getAttributes().getNamedItem("requiredHost").getTextContent();
				String requiredHostXPathExpression = getXPathExpression(requiredHost);

				try {
					expr = xpath.compile(requiredHostXPathExpression);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Node requiredHostNode = null;
				try {
					requiredHostNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
				} catch (XPathExpressionException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if (requiredHostNode.getAttributes().getNamedItem("name").getTextContent()!=null){
					if (requiredHostNode.getAttributes().getNamedItem("name").getTextContent().equalsIgnoreCase(requiredHostName)){
						providedHost = hostingNode.getAttributes().getNamedItem("providedHost").getTextContent();
						String providedHostXPathExpression = getXPathExpression(providedHost);
						try {
							expr = xpath.compile(providedHostXPathExpression);
						} catch (XPathExpressionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Node providedHostNode = null;
						try {
							providedHostNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
							if ((providedHostNode!=null)&&(providedHostNode.getAttributes()!=null)&&(providedHostNode.getAttributes().getNamedItem("name")!=null)){
								toHostName = providedHostNode.getAttributes().getNamedItem("name").getTextContent();
								return toHostName;
							}
						} catch (XPathExpressionException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}			
				}//end if
			}//end if
			index++;
		}//end while										
		return toHostName;
	}

	/**
	 * Inserts the capability match related part at the end of the model
	 * 
	 * @param strCamelWithHostingForSecurityAgent
	 * @param strSecurityAgentCamelModel
	 * @param agentName
	 * @param componentName
	 * @param strSecurityModelName
	 * @return
	 */
	private String setCapabilityMatchForSecurityAgent(String strCamelWithHostingForSecurityAgent,
			String strSecurityAgentCamelModel, String agentName, String componentName, String strSecurityModelName) {
		String strCapabilityMatchForSecurityAgent = "";
		/*
		 * capability match TSMEngineToMUSAAgentAC1 { from
		 * TSMEngine.TSMEngineACCapReq to MUSAACAgentForTSMEngine.CAP_AC }
		 * 
		 */
		String strHostingInfoDynamicallyGenerated = "\n\t\tcapability match " + componentName + "To" + agentName
				+ " {\n" + "\t\tfrom " + componentName + "." + componentName + "" + agentName + "CapReq to  MUSA"
				+ agentName + "For" + componentName + ".CAP_AC\n\t\t}";

		int indexOf = strCamelWithHostingForSecurityAgent.lastIndexOf("}");

		indexOf = strCamelWithHostingForSecurityAgent.substring(0, indexOf - 1).lastIndexOf("}");

		strCapabilityMatchForSecurityAgent = strCamelWithHostingForSecurityAgent.substring(0, indexOf - 1)
				+ strHostingInfoDynamicallyGenerated + "\n}\n}";

		return strCapabilityMatchForSecurityAgent;
	}

	/**
	 * Inserts the required security capability inside the internal component
	 * definition
	 * 
	 * @param strCamelWithCapabilityMatchForSecurityAgent
	 * @param strSecurityAgentCamelModel
	 * @param agentName
	 * @param componentName
	 * @param strSecurityModelName
	 * @return
	 */
	private String setRequiredSecurityCapability(String strCamelWithCapabilityMatchForSecurityAgent,
			String strSecurityAgentCamelModel, String agentName, String componentName, String strSecurityModelName) {
		String strModelWithRequiredSecurityCapability = "";
		// required security capability TSMEngineACCapReq
		// find the internal component definition by using regular expressions
		// insert the capability
		// return the modified model
		String strUpdatedCamel = "";
		int matchedIndex = 0;
		String strToInsert = "\n\t\t\trequired security capability " + componentName + "" + agentName + "CapReq\n";
		String subStringToSearch = "";

		int firstMatchedIndex = getMatchedEndIndex(strCamelWithCapabilityMatchForSecurityAgent,
				"internal component(\\s*)" + componentName+"(\\s*)(\\{)");

		int indexNexInternalComponent = strCamelWithCapabilityMatchForSecurityAgent.substring(firstMatchedIndex + 1)
				.indexOf("internal component");

		if (indexNexInternalComponent != -1) {
			subStringToSearch = strCamelWithCapabilityMatchForSecurityAgent.substring(0,
					firstMatchedIndex + indexNexInternalComponent);
			matchedIndex = getMatchedEndIndex(subStringToSearch,
					"internal component(\\s*)" + componentName + "([\\S\\s]*)IP public:(\\s*)(\\S*)");
			if (matchedIndex==0){
				matchedIndex = firstMatchedIndex;
			}

		} else {
			matchedIndex = getMatchedEndIndex(strCamelWithCapabilityMatchForSecurityAgent,
					"internal component(\\s*)" + componentName + "([\\S\\s]*)IP public:(\\s*)(\\S*)");
		}

		strModelWithRequiredSecurityCapability = strCamelWithCapabilityMatchForSecurityAgent.substring(0, matchedIndex)
				+ strToInsert + strCamelWithCapabilityMatchForSecurityAgent.substring(matchedIndex + 1);
		return strModelWithRequiredSecurityCapability;
	}

	/**
	 * Inserts a new security capability in the camel file
	 * 
	 * @param strCamel
	 * @param string
	 * @return
	 */
	private String setSecurityCapability(String strCamel, String securitycapability) {
		String strCamelWithNewSecurityCapability = "";
		// String securitycapability = "\n\nsecurity capability AGENTS_TEST {\n
		// controls [MUSASEC.AC-12]\n }\n";
		String strToBeInserted = ""; // first it is checked if exists a security
										// model in the camel
		int indexOf = strCamel.indexOf("security model");
		if (indexOf == -1) {
			strToBeInserted = "\n\nsecurity model SEC {\n\t" + securitycapability + "\t}";
			int matchedIndex = getMatchedEndIndex(strCamel, "camel model(\\s+)(\\S+)(\\s*)(\\{)");
			strCamelWithNewSecurityCapability = strCamel.substring(0, matchedIndex) + strToBeInserted
					+ strCamel.substring(matchedIndex) + 1;
		} else {
			strToBeInserted = securitycapability;
			int matchedIndex = getMatchedEndIndex(strCamel,
					"camel model(\\s)(\\S+)(\\s*)(\\{)[\\s\\S]*security model(\\s+)(\\S+)(\\s*)(\\{)");
			strCamelWithNewSecurityCapability = strCamel.substring(0, matchedIndex) + strToBeInserted
					+ strCamel.substring(matchedIndex) + 1;
		}
		return strCamelWithNewSecurityCapability;
	}

	/**
	 * This operation is called if components of an application are requested
	 * 
	 * @param appId
	 * @return
	 */
	@GET
	@Path("/getComponentsByApplication/{appId}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Components getComponentsByApplication(@PathParam("appId") String appId) {
		ManageMCAppDB baseDB = new ManageMCAppDB();
		ManageMCApp base = new ManageMCApp();
		Long lngAppId = new Long(appId);
		// String result = baseDB.checkIfAppExistsInAppComponentTable(lngAppId);
		Components res = base.getComponents(lngAppId);
		return res;
	}

	/**
	 * This operation is called if validation errors list is requested
	 * 
	 * @param appId
	 * @return
	 */
	@GET
	@Path("/getValidationErrorsByApplication/{appId}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ValidationErrors getValidationErrorsByApplication(@PathParam("appId") String appId) {
		/*
		 * try { Thread.sleep(milliseconds); // OJO: hay problemas de
		 * sincronizacion de servicios } catch (InterruptedException e) { //
		 * TODO Auto-generated catch block e.printStackTrace(); }
		 */
		ValidationErrors res = new ValidationErrors();
		ManageMCApp base = new ManageMCApp();
		Long lngAppId = new Long(appId);
		String resText = base.getValidationReport(lngAppId);
		if (resText == null || resText.length() == 0)
			return res;
		String[] list = resText.split("\n");
		for (String str : list) {
			ValidationError obj = new ValidationError();
			String[] data = str.split(":");
			if (data.length != 4)
				System.out.println("service getValidationErrorsByApplication: len error =." + data.length + ".");
			// obj.setId(data[0].trim());
			if (data.length > 0)
				obj.setLine(data[0].trim());
			if (data.length > 1)
				obj.setType(data[1].trim());
			if (data.length > 2)
				obj.setDescription(data[2].trim());
			res.addLinea(obj);
		}
		return res;
	}

	@GET
	@Path("/getModelWithAgentElements/{appId}")
	@Produces({ MediaType.TEXT_PLAIN })
	public String getModelWithAgentElements(@PathParam("appId") String appId) {
		ManageMCApp base = new ManageMCApp();
		Long lngAppId = new Long(appId);
		String res = "";

		return res;
	}

	/**
	 * This operation is called if validation errors status is requested
	 * 
	 * @param appId
	 * @return
	 */
	@GET
	@Path("/getValidationErrorsStatusByApplication/{appId}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Object getValidationErrorsStatusByApplication(@PathParam("appId") String appId) {
		/*
		 * try { Thread.sleep(milliseconds); // OJO: hay problemas de
		 * sincronizacion de servicios } catch (InterruptedException e) { //
		 * TODO Auto-generated catch block e.printStackTrace(); }
		 */
		Integer resInt = -1;
		ManageMCApp base = new ManageMCApp();
		Long lngAppId = new Long(appId);
		resInt = base.getErrorsValidationReport(lngAppId);
		Boolean resBool = false;
		if (resInt == 0)
			resBool = true;
		JSONObject json = new JSONObject();
		try {
			json.put("ValidationErrorStatus", resBool);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json.toString();
	}

	/**
	 * This method returns the camel corresponding to an application given its
	 * id. It can return a camel file or a xml file depending on the value of
	 * the format parameter.
	 * 
	 * @param appId
	 * @param format
	 * @return
	 */
	@GET
	@Path("/getCamel/{appId}/{format}")
	@Produces({ MediaType.TEXT_PLAIN + ";charset=utf-8" })
	public String getCamel(@PathParam("appId") String appId, @PathParam("format") String format) {
		Long lngAppId = new Long(appId);
		String strModel = "";
		String strCamelNoMUSACONF = "";
		String strMusaAgentsProcessed = "";
		ManageMCApp base = new ManageMCApp();
		strModel = base.getCamel(lngAppId);
		if ((strModel == null) || (strModel == ""))
			return ("ERROR - The application with ID = " + appId + " doesn�t exist in the database.");
		strCamelNoMUSACONF = strModel.replaceAll("MUSACONF.", "");
		// it�s neccesary to delete the last } in the file
		strCamelNoMUSACONF = strCamelNoMUSACONF.substring(0, strCamelNoMUSACONF.lastIndexOf("}") - 1);
		// new version returning stored camel file + camelagents configuration
		strMusaAgentsProcessed = getMusaAgentsStringContentProcessed();
		strModel = strCamelNoMUSACONF + strMusaAgentsProcessed;
		if (format.equalsIgnoreCase("CAMEL")) {

		} else if (format.equalsIgnoreCase("XML")) {
			strModel = getCamelInXMIFormat(strModel);
		} else if (format.equalsIgnoreCase("MACM")) {
			strModel = getCamelInMACMFormat(getCamelInXMIFormat(strModel), appId);
		} else {
			strModel = "The accepted values for the format are CAMEL, XML or MACM";
		}
		return strModel;
	}

	/**
	 * Returns the model in MACM format
	 * 
	 * @param camelInXMIFormat
	 * @param appId
	 * @return
	 */
	private String getCamelInMACMFormat(String camelInXMIFormat, String appId) {
		// xpath inizialitation
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document doc = null;
		String content = "";
		String strMACMModel;
		Long mcAppID = new Long(appId);
		// "camelDsl:CamelPlusModel" element name gives problems
		content = camelInXMIFormat.replace("camelDsl:CamelPlusModel", "camelDsl");

		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			doc = builder.parse(new InputSource(new StringReader(content)));
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		XPathExpression expr = null;

		if (!checkIfContainerExists(doc)) {
			strMACMModel = getCamelInMACMFormat_No_Container(camelInXMIFormat, appId);
			return (strMACMModel);
		}

		String allocationValue = getAllocationValue(doc);

		if ((allocationValue != null) && (allocationValue.equals("custom"))) {
			strMACMModel = camelToMACM_Allocation_Custom(doc, appId);
		}

		else {
			strMACMModel = camelToMACM_Allocation_NoCustom(doc, appId);
		}

		String strOrderedMACMModel = getOrderedMACMModel(strMACMModel);
		strMACMModel = strOrderedMACMModel.substring(0, strOrderedMACMModel.lastIndexOf(","));
		strMACMModel = "CREATE\n" + strMACMModel;
		return (strMACMModel);
	}

	/**
	 * Gets the maximum value of the component code in the hashmap
	 * 
	 * @param componentsIdsByApplication
	 * @return
	 */
	private int getComponentIdMaxValue(HashMap componentsIdsByApplication) {
		Collection values = componentsIdsByApplication.values();
		Iterator iterator = values.iterator();
		int value = 0;
		while (iterator.hasNext()) {
			String strComponentId = (String) iterator.next();
			int componentId = new Integer(strComponentId).intValue();
			if (componentId > value) {
				value = componentId;
			}
		}
		return value;
	}

	/**
	 * Returns the model in xmi format
	 * 
	 * @param strCamel
	 * @return model in xmi format
	 */
	private String getCamelInXMIFormat(String strCamel) {
		String strAuxFilePath = getMUSAAgentesPath() + "/auxCamelFile" + System.currentTimeMillis() + ".camel";
		PrintWriter out = null;
		try {
			out = new PrintWriter(strAuxFilePath);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		out.println(strCamel);
		out.flush();
		out.close();

		String strCamelXMIFormat = "";

		Injector injector = new CamelDslStandaloneSetup().createInjectorAndDoEMFRegistration();
		XtextResourceSet resourceSet = injector.getInstance(XtextResourceSet.class);

		String inputURI = "file:///" + strAuxFilePath;
		URI uri = URI.createURI(inputURI);
		Resource xtextResource = resourceSet.getResource(uri, true);

		EcoreUtil.resolveAll(xtextResource);

		EObject eObject = xtextResource.getContents().get(0);
		strCamelXMIFormat = serializeEObjectToXMI(eObject);

		// delete file
		File file = new File(strAuxFilePath);
		file.delete();
		return strCamelXMIFormat;
	}

	/**
	 * Returns the musa agents file modified so it can be included inside the
	 * master file.
	 * 
	 * @return
	 */
	private String getMusaAgentsStringContentProcessed() {
		String strMusaAgents = "";
		String strReturnValue = "";
		String musaAgentensPath = getMUSAAgentesPath();
		try {
			strMusaAgents = new String(Files.readAllBytes(Paths.get(musaAgentensPath + "/" + "MUSAAGENTS.camel")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// now it is necesary to delete the first line of the camel file and in
		// the last line replace "}}" by "}"
		strReturnValue = strMusaAgents.replaceFirst("camel model MUSACONF \\{", "");
		// it removes the last character in the string
		// strReturnValue = strMusaAgents.replace("}}", "}");
		return strReturnValue;
	}

	/**
	 * Returns the MUSA Agents path dependingo on the OS
	 * 
	 * @return
	 */
	private String getMUSAAgentesPath() {
		String musaAgentensPath = MUSAAGENTSLINUXPATH;
		if (System.getProperty("os.name").contains("Windows")) {
			musaAgentensPath = MUSAAGENTSWINDOWSPATH;
		}
		return musaAgentensPath;
	}

	/**
	 * This operation checks if the app exists in the Modeller Database
	 * 
	 * @param appId
	 * @return
	 */
	@GET
	@Path("/checkIfAppExistsInDB/{appId}")
	@Produces({ MediaType.TEXT_PLAIN })
	public String checkIfAppExistsInDB(@PathParam("appId") String appId) {
		String response = "false";
		Long lngAppId = new Long(appId);
		ManageMCApp base = new ManageMCApp();
		response = base.checkIfAppExistsInDB(lngAppId);
		return response;
	}

	/**
	 * Converts the given {@link EObject} model into its XMI representation.
	 * 
	 * @param model
	 *            The model to convert
	 * @return The XMI string representation of the given model
	 */
	public String serializeEObjectToXMI(final EObject model) {
		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getProtocolToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
		Resource resource = resourceSet.createResource(org.eclipse.emf.common.util.URI.createFileURI("temp/model.xmi"));
		EcoreUtil.resolveAll(resource);
		if (model != null) {
			resource.getContents().add(model);
		}
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			resource.save(baos, null);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		String modelXMI = baos.toString();
		try {
			baos.close();
		} catch (IOException ioe) {
		}
		if (model != null) {
			resource.getContents().remove(model);
		}
		try {
			resource.delete(null);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return modelXMI;
	}

	/**
	 * Performs the transformation CAMEL -> MACM for those models where
	 * allocation strategy != custom
	 * 
	 * @param doc
	 * @param appId
	 * @return
	 */
	private String camelToMACM_Allocation_NoCustom(Document doc, String appId) {
		// xpath inizialitation
		Long mcAppID = new Long(appId);
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		XPathExpression expr = null;
		StringBuffer strBuffer = new StringBuffer("");
		String vmName = "";
		String strVmRequirementSet = "";
		String strLocationRequirement = "";
		int lenght = 0;
		int index = 0;
		String returnValue = "";

		// SAAS
		try {
			expr = xpath.compile("camelDsl/deploymentModels/internalComponents");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		NodeList internalComponentsNodeList = null;
		int lenghtInternalComponentNodeList;
		int i;
		String componentName = null;
		String strComponentType = null;
		String strComponentTypeXPath = null;
		String requiredHostName = null;
		String strComponentName = null;
		String strMUSASecurityCapability = null;
		String strSecurityCapability = null;
		String strRequiredContainer = null;
		String strRequiredContainerXPath = null;
		String strContainerName = null;

		try {
			internalComponentsNodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		lenghtInternalComponentNodeList = internalComponentsNodeList.getLength();
		i = 0;
		componentName = "";
		strComponentType = "";
		strComponentTypeXPath = "";
		requiredHostName = "";
		strComponentName = "";
		ManageMCApp base = new ManageMCApp();
		HashMap<Object, Object> componentsIdsByApplication = base.getComponentsIdsByApplication(mcAppID);
		HashMap<Object, Object> componentsToBeInserted = new HashMap<>();
		int componentId = 0;
		while (i < lenghtInternalComponentNodeList) {
			componentName = "";
			strComponentType = "";
			strComponentTypeXPath = "";
			requiredHostName = "";
			strComponentName = "";
			strMUSASecurityCapability = "";
			strSecurityCapability = "";
			strRequiredContainer = "";
			strRequiredContainerXPath = "";
			strContainerName = "";

			Node internalComponent = internalComponentsNodeList.item(i);
			strComponentType = "";

			componentName = internalComponent.getAttributes().getNamedItem("name").getTextContent();
			if (internalComponent.getAttributes().getNamedItem("type") != null) { // 1
				strComponentType = internalComponent.getAttributes().getNamedItem("type").getTextContent();
				strComponentTypeXPath = getXPathExpression(strComponentType);
				try {
					expr = xpath.compile(strComponentTypeXPath);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Node musaComponentTypeNode = null;
				try {
					musaComponentTypeNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				strComponentName = musaComponentTypeNode.getAttributes().getNamedItem("name").getTextContent();
			} // 1

			if (internalComponent.getAttributes().getNamedItem("requiredContainer") != null) { // 6
				strRequiredContainer = internalComponent.getAttributes().getNamedItem("requiredContainer")
						.getTextContent();
				strRequiredContainerXPath = getXPathExpression(strRequiredContainer);
				try {
					expr = xpath.compile(strRequiredContainerXPath);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Node containerNode = null;
				try {
					containerNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if ((containerNode != null) && (containerNode.getAttributes() != null)
						&& (containerNode.getAttributes().getNamedItem("name") != null)
						&& (containerNode.getAttributes().getNamedItem("name").getTextContent() != null)) {
					strContainerName = containerNode.getAttributes().getNamedItem("name").getTextContent();
				}
			} // 6

			// set the information related to the provided security
			// (provMUSAsecurityCapability parameter)
			// <providedMUSACapabilities name="TSMEngineCap"
			// provMUSAsecurityCapability="//@securityModels.0/@MUSAsecurityCapabilities.1"/>
			// <MUSAsecurityCapabilities name="CAP2"
			// MUSAsecurityControls="//@securityModels.1/@MUSAsecurityControls.72
			// //@securityModels.1/@MUSAsecurityControls.191
			// //@securityModels.1/@MUSAsecurityControls.96"/>
			// <MUSAsecurityControls id="AC-1".............>
			try {
				// this expression selects the capabilities containing the
				// provMUSAsecurityCapability parameter
				expr = xpath.compile("camelDsl/deploymentModels/internalComponents[@name='" + componentName
						+ "']/providedMUSACapabilities[@provMUSAsecurityCapability]");

				Node providedMusaSecurityCapabilities = (Node) expr.evaluate(doc, XPathConstants.NODE);
				if (providedMusaSecurityCapabilities != null) {
					strMUSASecurityCapability = providedMusaSecurityCapabilities.getAttributes()
							.getNamedItem("provMUSAsecurityCapability").getTextContent();
				}
			} catch (XPathExpressionException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (strMUSASecurityCapability != "") {// 2

				strSecurityCapability = "";

				String musaSecurityCapabilityXPathExpression = getXPathExpression(strMUSASecurityCapability);

				try {
					expr = xpath.compile(musaSecurityCapabilityXPathExpression);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Node musaSecurityCapabilityNode = null;
				try {
					musaSecurityCapabilityNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
				} catch (XPathExpressionException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				String strMUSASecurityControls = "";
				StringBuffer strBufferSecurityControls = new StringBuffer("");

				if ((musaSecurityCapabilityNode != null) && (musaSecurityCapabilityNode.getAttributes() != null)
						&& (musaSecurityCapabilityNode.getAttributes().getNamedItem("MUSAsecurityControls") != null)
						&& (musaSecurityCapabilityNode.getAttributes().getNamedItem("MUSAsecurityControls")
								.getTextContent() != null)) {

					strMUSASecurityControls = musaSecurityCapabilityNode.getAttributes()
							.getNamedItem("MUSAsecurityControls").getTextContent();

					// there coulb be more than 1 controls
					String[] strMUSASecurityControlsSplitted = strMUSASecurityControls.split(" ");
					for (int j = 0; j < strMUSASecurityControlsSplitted.length; j++) {
						String strSecurityControl = strMUSASecurityControlsSplitted[j];
						String strSecurityControlXPathExpression = getXPathExpression(strSecurityControl);
						try {
							expr = xpath.compile(strSecurityControlXPathExpression);
						} catch (XPathExpressionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Node musaSecurityControlNode = null;
						try {
							musaSecurityControlNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
						} catch (XPathExpressionException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						if (musaSecurityControlNode != null) {
							strBufferSecurityControls.append(
									musaSecurityControlNode.getAttributes().getNamedItem("id").getTextContent());
							if (j != strMUSASecurityControlsSplitted.length - 1) {
								strBufferSecurityControls.append(", ");
							}
						}
						strSecurityCapability = strBufferSecurityControls.toString();
					} // end for

				} // end if no security controls
			} // 2
				// logic included in order to set the value of the component id

			if (componentsIdsByApplication.containsKey(componentName)) {
				componentId = new Integer((String) componentsIdsByApplication.get(componentName)).intValue();
			} else {
				componentId = getComponentIdMaxValue(componentsIdsByApplication) + 1;
				componentsToBeInserted.put(componentName, new Integer(componentId).toString());
				componentsIdsByApplication.put(componentName, new Integer(componentId).toString());
			}
			strBuffer.append("(" + componentName + ":SaaS:service {name:'" + componentName + "',type:'"
					+ strComponentName + "', seccap_provided:'" + strSecurityCapability + "', app_id:'" + appId
					+ "', component_id:'" + componentId + "'})\n");
			if ((strContainerName != "") && (strContainerName != null) && (componentName != "")
					&& (componentName != null)) {
				strBuffer.append("(" + strContainerName + ")-[:hosts {name:'" + strContainerName + "'}]->("
						+ componentName + ")\n");
			}
			// "(webapp1:SaaS:service {name:'webapp1',type:'webapp'})";
			// "(db1:SaaS:service {name:'db1', type:'db',
			// seccap_provided:�AC-11(1), AU-13(2)�})";
			i++;
		} // end while

		// GEV commented in nov cos it is done in updateComponentsPerAppliction
		// method
		// base.insertMissingComponentCodes(mcAppID, componentsToBeInserted);

		// RELATIONS
		/*
		 * we consider that a component uses other component if the first one
		 * requires a communication (requiredCommunications parameter) offered
		 * by other component (providedCommunication parameter) example
		 * TSMEngine requires IDManagerPort
		 *****
		 * <internalComponents name="TSMEngine" type="//@musaComponentTypes.2"
		 * order="3"> <providedCommunications name="TSMEnginePort"
		 * portNumber="8185"/> <providedCommunications name="TSMEngineRESTPort"
		 * portNumber="443"/> <configurations name="TSMEngineConfigurationCHEF">
		 * <confManager name="C1" cookbook="'tut'" recipe="'musa_tsme'"/>
		 * </configurations> <requiredCommunications name="IDManagerPortReq"
		 * portNumber="3000" isMandatory="true"/> <requiredCommunications
		 * name="ConsumptionEstimatorPortReq" portNumber="9090"
		 * isMandatory="true"/> <requiredCommunications
		 * name="JourneyPlannerPortReq" portNumber="8085" isMandatory="true"/>
		 * <requiredCommunications name="DatabasePortReq" portNumber="3306"
		 * isMandatory="true"/> <requiredHost
		 * name="CoreIntensiveUbuntuFinlandHostReq"/> <requiredMUSACapabilities
		 * name="MUSAAgentACCapReq" reqMUSAsecurityCapability=
		 * "//@securityModels.0/@MUSAsecurityCapabilities.0"/>
		 * <providedMUSACapabilities name="TSMEngineCap"
		 * provMUSAsecurityCapability=
		 * "//@securityModels.0/@MUSAsecurityCapabilities.1"/>
		 * </internalComponents>
		 * 
		 * IDManagerPort is offered by IDMAM
		 ********
		 * <internalComponents name="IDMAM"> <providedCommunications
		 * name="IDManagerPort" portNumber="3000"/> <providedCommunications
		 * name="MongoDBPort" portNumber="27017"/> <configurations
		 * name="IDManagerManualConfiguration"> <confManager name="C1"
		 * cookbook="'tut'" recipe="'musa_idm'"/> </configurations>
		 * <requiredHost name="StorageIntensiveUbuntuFinlandHostReq"/>
		 * </internalComponents>
		 */

		// PAAS
		// (poolTSM:IaaS:service {name:'poolTSM', type:'pool',
		// array_vms:'CPUIntensiveUbuntuUKVM' 'CPUIntensiveUbuntuFinlandVM'}),
		try {
			expr = xpath.compile("camelDsl/deploymentModels/pools");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		NodeList poolsNodeList = null;
		try {
			poolsNodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		lenght = poolsNodeList.getLength();
		index = 0;
		int j = 0;
		String poolName;
		String strVms;
		String strAllVms;
		String strVmXPathExpression = getXPathExpression(strVmRequirementSet);

		while (index < lenght) {
			poolName = "";
			strVms = "";
			strAllVms = "";
			Node poolNode = poolsNodeList.item(index);
			poolName = poolNode.getAttributes().getNamedItem("name").getTextContent();
			strVms = poolNode.getAttributes().getNamedItem("requiredHosts").getTextContent();
			StringTokenizer tokens = new StringTokenizer(strVms, " ");
			j = 0;
			while (tokens.hasMoreTokens()) {
				strVmXPathExpression = getXPathExpression(tokens.nextToken());
				try {
					expr = xpath.compile(strVmXPathExpression);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Node vmProvidedHostNode = null;
				try {
					vmProvidedHostNode = (Node) expr.evaluate(doc, XPathConstants.NODE);

					if ((vmProvidedHostNode != null) && (vmProvidedHostNode.getAttributes() != null)
							&& (vmProvidedHostNode.getAttributes().getNamedItem("name") != null)
							&& (vmProvidedHostNode.getAttributes().getNamedItem("name").getTextContent() != null)) {
						if (j > 0)
							strAllVms += ", ";
						strAllVms += vmProvidedHostNode.getParentNode().getAttributes().getNamedItem("name")
								.getTextContent();
					}
				} catch (XPathExpressionException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				j++;
				// strLocationRequirement =
				// vmRequirementSetsNode.getAttributes().getNamedItem("locationRequirement").getTextContent();
			}

			index++;
		} // end while

		// (containerTSM:PaaS:service {name:'containerTSM', type:'docker_swarm',
		// allocationStrategy: 'custom'}),
		// (poolTSM)-[:hosts {name:'poolTSM',
		// manager:'CPUIntensiveUbuntuUKVM'}]->(containerTSM),
		// *********I can�t get type from the xmi file !! It doen�t appear
		try {
			expr = xpath.compile("camelDsl/deploymentModels/containers[1]");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Node containerNode = null;
		try {
			containerNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String containerName = "";
		String allocationStrategy = "";
		String strPoolHost = "";
		String strPoolName = "";
		String strPoolVMHost = "";
		String strPoolVMName = "";
		String requiredHosts = "";
		String requiredVM = "";
		String requiredHost = "";
		String strCountryId = "";

		if ((containerNode != null) && (containerNode.getAttributes() != null)
				&& (containerNode.getAttributes().getNamedItem("name") != null)
				&& (containerNode.getAttributes().getNamedItem("name").getTextContent() != null)) {
			containerName = containerNode.getAttributes().getNamedItem("name").getTextContent();
			allocationStrategy = containerNode.getAttributes().getNamedItem("allocationStrategyEnum").getTextContent();
			strPoolHost = containerNode.getAttributes().getNamedItem("requiredPoolHost").getTextContent();
			strPoolVMHost = containerNode.getAttributes().getNamedItem("requiredPoolVMHost").getTextContent();
			strBuffer.append("(" + containerName + ":PaaS:service {name:'" + containerName
					+ "', type:'docker_swarm', allocationStrategy: '" + allocationStrategy + "', app_id:'" + appId
					+ "'})\n");

			// index++;
			String poolHostXPathExpression = getXPathExpression(strPoolHost);
			try {
				expr = xpath.compile(poolHostXPathExpression);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Node poolHostNode = null;
			try {
				poolHostNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
				if ((poolHostNode != null) && (poolHostNode.getAttributes() != null)
						&& (poolHostNode.getAttributes().getNamedItem("name") != null)
						&& (poolHostNode.getAttributes().getNamedItem("name").getTextContent() != null)) {
					strPoolName = poolHostNode.getAttributes().getNamedItem("name").getTextContent();
					requiredHosts = poolHostNode.getAttributes().getNamedItem("requiredHosts").getTextContent();
					// poolTSM
				}

			} catch (XPathExpressionException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			StringTokenizer st = new StringTokenizer(requiredHosts, " ");

			while (st.hasMoreTokens()) {
				requiredHost = st.nextToken();
				requiredVM = requiredHost.substring(0, requiredHost.lastIndexOf("/"));
				String requiredVMXPathExpression = getXPathExpression(requiredVM);
				String requiredHostXPathExpression = getXPathExpression(requiredHost);
				// requiredHostName
				try {
					expr = xpath.compile(requiredHostXPathExpression);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Node requiredHostNode = null;

				try {
					requiredHostNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
				} catch (XPathExpressionException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				// CPUIntensiveUbuntuUKHost
				if ((requiredHostNode != null) && (requiredHostNode.getAttributes() != null)
						&& (requiredHostNode.getAttributes().getNamedItem("name") != null)
						&& (requiredHostNode.getAttributes().getNamedItem("name").getTextContent() != null)) {
					requiredHostName = requiredHostNode.getAttributes().getNamedItem("name").getTextContent();
				}

				try {
					expr = xpath.compile(requiredVMXPathExpression);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Node vmNode = null;
				try {
					vmNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
					// CPUIntensiveUbuntuUKHost
					if ((vmNode != null) && (vmNode.getAttributes() != null)
							&& (vmNode.getAttributes().getNamedItem("name") != null)
							&& (vmNode.getAttributes().getNamedItem("name").getTextContent() != null)) {
						vmName = vmNode.getAttributes().getNamedItem("name").getTextContent();
						strVmRequirementSet = vmNode.getAttributes().getNamedItem("vmRequirementSet").getTextContent();
						String vmRequirementSerXPathExpression = getXPathExpression(strVmRequirementSet);
						// get location

						try {
							expr = xpath.compile(vmRequirementSerXPathExpression);
						} catch (XPathExpressionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Node vmRequirementSetsNode = null;
						try {
							vmRequirementSetsNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
						} catch (XPathExpressionException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						if ((vmRequirementSetsNode != null) && (vmRequirementSetsNode.getAttributes() != null)
								&& (vmRequirementSetsNode.getAttributes()
										.getNamedItem("locationRequirement") != null)) {
							strLocationRequirement = vmRequirementSetsNode.getAttributes()
									.getNamedItem("locationRequirement").getTextContent();
						}
						String locationRequirementXPathExpression = getXPathExpression(strLocationRequirement);
						try {
							expr = xpath.compile(locationRequirementXPathExpression);
						} catch (XPathExpressionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Node requirementsNode = null;
						try {
							requirementsNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
						} catch (XPathExpressionException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						String strRequirementLocations = requirementsNode.getAttributes().getNamedItem("locations")
								.getTextContent();
						String strRequirementLocationsXPathExpression = getXPathExpression(strRequirementLocations);

						try {
							expr = xpath.compile(strRequirementLocationsXPathExpression);
						} catch (XPathExpressionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Node countriesNode = null;
						try {
							countriesNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
						} catch (XPathExpressionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						strCountryId = "";
						if ((countriesNode != null) && (countriesNode.getAttributes() != null)
								&& (countriesNode.getAttributes().getNamedItem("id") != null)) {
							strCountryId = countriesNode.getAttributes().getNamedItem("id").getTextContent();
						}
					}

				} catch (XPathExpressionException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		} // end IF if ((containerNode != null)
			// change for CUSTOM RANDOM
		if ((strPoolName != null) && (strPoolName != "") && (strPoolVMName != null) && (strPoolVMName != "")
				&& (containerName != null) && (containerName != ""))
			strBuffer.append("(" + strPoolName + ")-[:hosts {name:'" + strPoolName + "', manager:'" + strPoolVMName
					+ "'}]->(" + containerName + ")\n");

		try {
			expr = xpath.compile("camelDsl/deploymentModels/internalComponents");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String requiredCommunicationName = "";

		try {
			internalComponentsNodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		lenghtInternalComponentNodeList = internalComponentsNodeList.getLength();
		Node internalComponent = null;
		NodeList internalComponentChildNodes = null;
		NodeList serviceProviderInternalComponentChildNodes = null;
		int internalComponentChildNodesLength;
		int servideProviderInternalComponentChildNodesLength;
		// String usedInternalComponentName = "";

		for (i = 0; i < lenghtInternalComponentNodeList; i++) {
			internalComponent = internalComponentsNodeList.item(i);
			strComponentType = "";
			componentName = internalComponent.getAttributes().getNamedItem("name").getTextContent();

			internalComponentChildNodes = internalComponent.getChildNodes();
			internalComponentChildNodesLength = internalComponentChildNodes.getLength();
			for (int k = 0; k < internalComponentChildNodesLength; k++) {
				Node itemk = internalComponentChildNodes.item(k);

				// new
				if (itemk.getNodeName().equalsIgnoreCase("requiredHost")) {
					requiredHostName = itemk.getAttributes().getNamedItem("name").getTextContent();
					// StorageIntensiveUbuntuFinlandHostReq
					requiredHostName = requiredHostName.replaceAll("Req", "");

					// requiredHostName =
					// requiredCommunicationName.replaceAll("Req", "");

					try {
						expr = xpath.compile(
								"camelDsl/deploymentModels/vms/providedHosts[@name='" + requiredHostName + "']");
					} catch (XPathExpressionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					Node providedHostNode = null;
					try {
						providedHostNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
						if (providedHostNode != null)
							vmName = providedHostNode.getParentNode().getAttributes().getNamedItem("name")
									.getTextContent();
					} catch (XPathExpressionException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					if ((vmName != null) && (vmName != "") && (containerName != null) && (containerName != "")
							&& (requiredHostName != null) && (requiredHostName != "") && (componentName != null)
							&& (componentName != ""))
						strBuffer.append("(" + containerName + ")-[:hosts {name:'" + requiredHostName + "'}]->("
								+ componentName + ")\n");

				} // end if

			} // end for

		} // end for

		// *************************communications
		NodeList communicationsNodeList = null;
		String strRequiredCommunication = "";
		String strProvidedCommunication = "";
		try {
			expr = xpath.compile("camelDsl/deploymentModels/communications");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			communicationsNodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		int lenghtCommunicationsNodeList = 0;
		if (communicationsNodeList != null) {
			lenghtCommunicationsNodeList = communicationsNodeList.getLength();
		}
		Node communicationNode = null;
		String internalComponentName = "";
		String usedInternalComponentName = "";

		for (i = 0; i < lenghtCommunicationsNodeList; i++) {
			communicationNode = communicationsNodeList.item(i);
			if ((communicationNode != null) && (communicationNode.getAttributes() != null)) {
				if (communicationNode.getAttributes().getNamedItem("requiredCommunication") != null)
					strRequiredCommunication = communicationNode.getAttributes().getNamedItem("requiredCommunication")
							.getTextContent();
				if (communicationNode.getAttributes().getNamedItem("providedCommunication") != null)
					strProvidedCommunication = communicationNode.getAttributes().getNamedItem("providedCommunication")
							.getTextContent();
			}
			String strRequiredCommunicationXPathExpression = getXPathExpression(strRequiredCommunication);
			String strProvidedCommunicationXPathExpression = getXPathExpression(strProvidedCommunication);
			// to get internalComponentName
			try {
				expr = xpath.compile(strRequiredCommunicationXPathExpression);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Node requiredCommunicationNode = null;

			try {
				requiredCommunicationNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if ((requiredCommunicationNode != null) && (requiredCommunicationNode.getParentNode() != null)
					&& (requiredCommunicationNode.getParentNode().getAttributes() != null)
					&& (requiredCommunicationNode.getParentNode().getAttributes().getNamedItem("name") != null)) {
				internalComponentName = requiredCommunicationNode.getParentNode().getAttributes().getNamedItem("name")
						.getTextContent();
			}

			// to get usedInternalComponentName
			try {
				expr = xpath.compile(strProvidedCommunicationXPathExpression);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Node providedCommunicationNode = null;

			try {
				providedCommunicationNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if ((providedCommunicationNode != null) && (providedCommunicationNode.getParentNode() != null)
					&& (providedCommunicationNode.getParentNode().getAttributes() != null)
					&& (providedCommunicationNode.getParentNode().getAttributes().getNamedItem("name") != null)) {
				usedInternalComponentName = providedCommunicationNode.getParentNode().getAttributes()
						.getNamedItem("name").getTextContent();
			}

			strBuffer.append(
					"(" + internalComponentName + ")-[:uses {name:'uses'}]->(" + usedInternalComponentName + ")\n");
		}

		NodeList capabilityLinksNodeList = null;
		String strRequiredMUSACapability = "";
		String strProvidedMUSACapability = "";
		try {
			expr = xpath.compile("camelDsl/deploymentModels/capabilityLinks");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			capabilityLinksNodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int lenghtCapabilityLinksNodeList = 0;
		if (capabilityLinksNodeList != null) {
			lenghtCapabilityLinksNodeList = capabilityLinksNodeList.getLength();
		}
		Node capabilityLinksNode = null;
		internalComponentName = "";
		usedInternalComponentName = "";

		for (i = 0; i < lenghtCapabilityLinksNodeList; i++) {
			capabilityLinksNode = capabilityLinksNodeList.item(i);
			if (capabilityLinksNode.getAttributes() != null) {
				if (capabilityLinksNode.getAttributes().getNamedItem("requiredMUSACapability") != null)
					strRequiredMUSACapability = capabilityLinksNode.getAttributes()
							.getNamedItem("requiredMUSACapability").getTextContent();
				if (capabilityLinksNode.getAttributes().getNamedItem("providedMUSACapability") != null)
					strProvidedMUSACapability = capabilityLinksNode.getAttributes()
							.getNamedItem("providedMUSACapability").getTextContent();
			}
			String strRequiredMUSACapabilityXPathExpression = getXPathExpression(strRequiredMUSACapability);
			String strProvidedMUSACapabilityXPathExpression = getXPathExpression(strProvidedMUSACapability);
			// to get internalComponentName
			try {
				expr = xpath.compile(strRequiredMUSACapabilityXPathExpression);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Node requiredMUSACapabilitiesNode = null;

			try {
				requiredMUSACapabilitiesNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if ((requiredMUSACapabilitiesNode != null) && (requiredMUSACapabilitiesNode.getParentNode() != null)
					&& (requiredMUSACapabilitiesNode.getParentNode().getAttributes() != null)
					&& (requiredMUSACapabilitiesNode.getParentNode().getAttributes().getNamedItem("name") != null)) {
				internalComponentName = requiredMUSACapabilitiesNode.getParentNode().getAttributes()
						.getNamedItem("name").getTextContent();
			}

			// to get usedInternalComponentName
			try {
				expr = xpath.compile(strProvidedMUSACapabilityXPathExpression);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Node providedMUSACapabilitiesNode = null;

			try {
				providedMUSACapabilitiesNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if ((providedMUSACapabilitiesNode != null) && (providedMUSACapabilitiesNode.getParentNode() != null)
					&& (providedMUSACapabilitiesNode.getParentNode().getAttributes() != null)
					&& (providedMUSACapabilitiesNode.getParentNode().getAttributes().getNamedItem("name") != null)) {
				usedInternalComponentName = providedMUSACapabilitiesNode.getParentNode().getAttributes()
						.getNamedItem("name").getTextContent();
			}

			strBuffer.append(
					"(" + internalComponentName + ")-[:uses {name:'uses'}]->(" + usedInternalComponentName + ")\n");
		}

		// ************************* hostings
		String poolProviderHost;
		try {
			expr = xpath.compile("camelDsl/deploymentModels/hostings");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		NodeList hostingsNodeList = null;
		try {
			hostingsNodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		lenght = hostingsNodeList.getLength();
		index = 0;
		int k = 0;
		while (index < lenght) {
			strPoolName = "";
			Node hostingNode = hostingsNodeList.item(index);
			if (hostingNode.getAttributes().getNamedItem("poolProvidedHost") != null) {
				poolProviderHost = hostingNode.getAttributes().getNamedItem("poolProvidedHost").getTextContent();
				StringTokenizer tokens = new StringTokenizer(poolProviderHost, " ");
				// k = 0;
				while (tokens.hasMoreTokens()) {
					String strpoolsXPathExpression = getXPathExpression(tokens.nextToken());
					try {
						expr = xpath.compile(strpoolsXPathExpression);
					} catch (XPathExpressionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					Node providedHostNode = null;
					try {
						providedHostNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
						strPoolName = providedHostNode.getParentNode().getAttributes().getNamedItem("name")
								.getTextContent();
					} catch (XPathExpressionException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} // end while
				requiredHost = hostingNode.getAttributes().getNamedItem("requiredHost").getTextContent();
				tokens = new StringTokenizer(requiredHost, " ");

				while (tokens.hasMoreTokens()) {
					String internalComponentXPathExpression = getXPathExpression(tokens.nextToken());
					try {
						expr = xpath.compile(internalComponentXPathExpression);
					} catch (XPathExpressionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					Node requiredHostHostNode = null;
					try {
						requiredHostHostNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
						strContainerName = requiredHostHostNode.getParentNode().getAttributes().getNamedItem("name")
								.getTextContent();
					} catch (XPathExpressionException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} // end while
					// change for CUSTOM ALLOCATION
				/*
				 * strBuffer.append( "(" + strPoolName + ")-[:hosts {name:'" +
				 * strPoolName + "'}]->(" + strContainerName + ")\n");
				 */
			} // end if

			// (poolTSM)-[:hosts {name:'poolTSM'}]->(JourneyPlanner)
			index++;
		} // end while
			// ******************************

		returnValue = strBuffer.toString();
		return returnValue;
	}

	/**
	 * Performs the transformation CAMEL -> MACM for those models where
	 * allocation strategy = custom
	 * 
	 * @param doc
	 * @param appId
	 * @return
	 */
	private String camelToMACM_Allocation_Custom(Document doc, String appId) {
		// xpath inizialitation
		Long mcAppID = new Long(appId);
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		XPathExpression expr = null;
		StringBuffer strBuffer = new StringBuffer("");
		String vmName = "";
		String strVmRequirementSet = "";
		String strLocationRequirement = "";
		String strQuantitativeHardwareRequirement = "";
		int lenght = 0;
		int index = 0;
		String returnValue = "";

		// SAAS
		try {
			expr = xpath.compile("camelDsl/deploymentModels/internalComponents");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		NodeList internalComponentsNodeList = null;
		int lenghtInternalComponentNodeList;
		int i;
		String componentName = null;
		String strComponentType = null;
		String strComponentTypeXPath = null;
		String requiredHostName = null;
		String strComponentName = null;
		String strMUSASecurityCapability = null;
		String strSecurityCapability = null;
		String strRequiredContainer = null;
		String strRequiredContainerXPath = null;
		String strContainerName = null;

		try {
			internalComponentsNodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		lenghtInternalComponentNodeList = internalComponentsNodeList.getLength();
		i = 0;
		componentName = "";
		strComponentType = "";
		strComponentTypeXPath = "";
		requiredHostName = "";
		strComponentName = "";
		ManageMCApp base = new ManageMCApp();
		HashMap<Object, Object> componentsIdsByApplication = base.getComponentsIdsByApplication(mcAppID);
		HashMap<Object, Object> componentsToBeInserted = new HashMap<>();
		int componentId = 0;
		while (i < lenghtInternalComponentNodeList) {
			componentName = "";
			strComponentType = "";
			strComponentTypeXPath = "";
			requiredHostName = "";
			strComponentName = "";
			strMUSASecurityCapability = "";
			strSecurityCapability = "";
			strRequiredContainer = "";
			strRequiredContainerXPath = "";
			strContainerName = "";

			Node internalComponent = internalComponentsNodeList.item(i);
			strComponentType = "";

			componentName = internalComponent.getAttributes().getNamedItem("name").getTextContent();
			if (internalComponent.getAttributes().getNamedItem("type") != null) { // 1
				strComponentType = internalComponent.getAttributes().getNamedItem("type").getTextContent();
				strComponentTypeXPath = getXPathExpression(strComponentType);
				try {
					expr = xpath.compile(strComponentTypeXPath);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Node musaComponentTypeNode = null;
				try {
					musaComponentTypeNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				strComponentName = musaComponentTypeNode.getAttributes().getNamedItem("name").getTextContent();
			} // 1

			if (internalComponent.getAttributes().getNamedItem("requiredContainer") != null) { // 6
				strRequiredContainer = internalComponent.getAttributes().getNamedItem("requiredContainer")
						.getTextContent();
				strRequiredContainerXPath = getXPathExpression(strRequiredContainer);
				try {
					expr = xpath.compile(strRequiredContainerXPath);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Node containerNode = null;
				try {
					containerNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if ((containerNode != null) && (containerNode.getAttributes() != null)
						&& (containerNode.getAttributes().getNamedItem("name") != null)
						&& (containerNode.getAttributes().getNamedItem("name").getTextContent() != null)) {
					strContainerName = containerNode.getAttributes().getNamedItem("name").getTextContent();
				}
			} // 6

			// set the information related to the provided security
			// (provMUSAsecurityCapability parameter)
			// <providedMUSACapabilities name="TSMEngineCap"
			// provMUSAsecurityCapability="//@securityModels.0/@MUSAsecurityCapabilities.1"/>
			// <MUSAsecurityCapabilities name="CAP2"
			// MUSAsecurityControls="//@securityModels.1/@MUSAsecurityControls.72
			// //@securityModels.1/@MUSAsecurityControls.191
			// //@securityModels.1/@MUSAsecurityControls.96"/>
			// <MUSAsecurityControls id="AC-1".............>
			try {
				// this expression selects the capabilities containing the
				// provMUSAsecurityCapability parameter
				expr = xpath.compile("camelDsl/deploymentModels/internalComponents[@name='" + componentName
						+ "']/providedMUSACapabilities[@provMUSAsecurityCapability]");

				Node providedMusaSecurityCapabilities = (Node) expr.evaluate(doc, XPathConstants.NODE);
				if (providedMusaSecurityCapabilities != null) {
					strMUSASecurityCapability = providedMusaSecurityCapabilities.getAttributes()
							.getNamedItem("provMUSAsecurityCapability").getTextContent();
				}
			} catch (XPathExpressionException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (strMUSASecurityCapability != "") {// 2

				strSecurityCapability = "";

				String musaSecurityCapabilityXPathExpression = getXPathExpression(strMUSASecurityCapability);

				try {
					expr = xpath.compile(musaSecurityCapabilityXPathExpression);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Node musaSecurityCapabilityNode = null;
				try {
					musaSecurityCapabilityNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
				} catch (XPathExpressionException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				String strMUSASecurityControls = "";
				StringBuffer strBufferSecurityControls = new StringBuffer("");

				if ((musaSecurityCapabilityNode != null) && (musaSecurityCapabilityNode.getAttributes() != null)
						&& (musaSecurityCapabilityNode.getAttributes().getNamedItem("MUSAsecurityControls") != null)
						&& (musaSecurityCapabilityNode.getAttributes().getNamedItem("MUSAsecurityControls")
								.getTextContent() != null)) {

					strMUSASecurityControls = musaSecurityCapabilityNode.getAttributes()
							.getNamedItem("MUSAsecurityControls").getTextContent();

					// there coulb be more than 1 controls
					String[] strMUSASecurityControlsSplitted = strMUSASecurityControls.split(" ");
					for (int j = 0; j < strMUSASecurityControlsSplitted.length; j++) {
						String strSecurityControl = strMUSASecurityControlsSplitted[j];
						String strSecurityControlXPathExpression = getXPathExpression(strSecurityControl);
						try {
							expr = xpath.compile(strSecurityControlXPathExpression);
						} catch (XPathExpressionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Node musaSecurityControlNode = null;
						try {
							musaSecurityControlNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
						} catch (XPathExpressionException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						if (musaSecurityControlNode != null) {
							strBufferSecurityControls.append(
									musaSecurityControlNode.getAttributes().getNamedItem("id").getTextContent());
							if (j != strMUSASecurityControlsSplitted.length - 1) {
								strBufferSecurityControls.append(", ");
							}
						}
						strSecurityCapability = strBufferSecurityControls.toString();
					} // end for

				} // end if no security controls
			} // 2
				// //logic included in order to set the value of the component
				// id

			if (componentsIdsByApplication.containsKey(componentName)) {
				componentId = new Integer((String) componentsIdsByApplication.get(componentName)).intValue();
			} else {
				componentId = getComponentIdMaxValue(componentsIdsByApplication) + 1;
				componentsToBeInserted.put(componentName, new Integer(componentId).toString());
				componentsIdsByApplication.put(componentName, new Integer(componentId).toString());
			}
			strBuffer.append("(" + componentName + ":SaaS:service {name:'" + componentName + "',type:'"
					+ strComponentName + "', seccap_provided:'" + strSecurityCapability + "', app_id:'" + appId
					+ "', component_id:'" + componentId + "'})\n");
			if ((strContainerName != "") && (strContainerName != null) && (componentName != "")
					&& (componentName != null)) {
				strBuffer.append("(" + strContainerName + ")-[:hosts {name:'" + strContainerName + "'}]->("
						+ componentName + ")\n");
			}
			// "(webapp1:SaaS:service {name:'webapp1',type:'webapp'})";
			// "(db1:SaaS:service {name:'db1', type:'db',
			// seccap_provided:�AC-11(1), AU-13(2)�})";
			i++;
		} // end while
			// GEV commented in nov cos it is done in
			// updateComponentsPerAppliction method
			// base.insertMissingComponentCodes(mcAppID,
			// componentsToBeInserted);

		// RELATIONS
		/*
		 * we consider that a component uses other component if the first one
		 * requires a communication (requiredCommunications parameter) offered
		 * by other component (providedCommunication parameter) example
		 * TSMEngine requires IDManagerPort
		 *****
		 * <internalComponents name="TSMEngine" type="//@musaComponentTypes.2"
		 * order="3"> <providedCommunications name="TSMEnginePort"
		 * portNumber="8185"/> <providedCommunications name="TSMEngineRESTPort"
		 * portNumber="443"/> <configurations name="TSMEngineConfigurationCHEF">
		 * <confManager name="C1" cookbook="'tut'" recipe="'musa_tsme'"/>
		 * </configurations> <requiredCommunications name="IDManagerPortReq"
		 * portNumber="3000" isMandatory="true"/> <requiredCommunications
		 * name="ConsumptionEstimatorPortReq" portNumber="9090"
		 * isMandatory="true"/> <requiredCommunications
		 * name="JourneyPlannerPortReq" portNumber="8085" isMandatory="true"/>
		 * <requiredCommunications name="DatabasePortReq" portNumber="3306"
		 * isMandatory="true"/> <requiredHost
		 * name="CoreIntensiveUbuntuFinlandHostReq"/> <requiredMUSACapabilities
		 * name="MUSAAgentACCapReq" reqMUSAsecurityCapability=
		 * "//@securityModels.0/@MUSAsecurityCapabilities.0"/>
		 * <providedMUSACapabilities name="TSMEngineCap"
		 * provMUSAsecurityCapability=
		 * "//@securityModels.0/@MUSAsecurityCapabilities.1"/>
		 * </internalComponents>
		 * 
		 * IDManagerPort is offered by IDMAM
		 ********
		 * <internalComponents name="IDMAM"> <providedCommunications
		 * name="IDManagerPort" portNumber="3000"/> <providedCommunications
		 * name="MongoDBPort" portNumber="27017"/> <configurations
		 * name="IDManagerManualConfiguration"> <confManager name="C1"
		 * cookbook="'tut'" recipe="'musa_idm'"/> </configurations>
		 * <requiredHost name="StorageIntensiveUbuntuFinlandHostReq"/>
		 * </internalComponents>
		 */

		// PAAS
		// (poolTSM:IaaS:service {name:'poolTSM', type:'pool',
		// array_vms:'CPUIntensiveUbuntuUKVM' 'CPUIntensiveUbuntuFinlandVM'}),
		try {
			expr = xpath.compile("camelDsl/deploymentModels/pools");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		NodeList poolsNodeList = null;
		try {
			poolsNodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		lenght = poolsNodeList.getLength();
		index = 0;
		int j = 0;
		String poolName;
		String strVms;
		String strAllVms;
		String strVmXPathExpression = getXPathExpression(strVmRequirementSet);

		while (index < lenght) {
			poolName = "";
			strVms = "";
			strAllVms = "";
			Node poolNode = poolsNodeList.item(index);
			poolName = poolNode.getAttributes().getNamedItem("name").getTextContent();
			strVms = poolNode.getAttributes().getNamedItem("requiredHosts").getTextContent();
			StringTokenizer tokens = new StringTokenizer(strVms, " ");
			j = 0;
			while (tokens.hasMoreTokens()) {
				strVmXPathExpression = getXPathExpression(tokens.nextToken());
				try {
					expr = xpath.compile(strVmXPathExpression);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Node vmProvidedHostNode = null;
				try {
					vmProvidedHostNode = (Node) expr.evaluate(doc, XPathConstants.NODE);

					if ((vmProvidedHostNode != null) && (vmProvidedHostNode.getAttributes() != null)
							&& (vmProvidedHostNode.getAttributes().getNamedItem("name") != null)
							&& (vmProvidedHostNode.getAttributes().getNamedItem("name").getTextContent() != null)) {
						if (j > 0)
							strAllVms += ", ";
						strAllVms += vmProvidedHostNode.getParentNode().getAttributes().getNamedItem("name")
								.getTextContent();
					}
				} catch (XPathExpressionException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				j++;
				// strLocationRequirement =
				// vmRequirementSetsNode.getAttributes().getNamedItem("locationRequirement").getTextContent();
			}

			// change for CUSTOM ALLOCATION
			/*
			 * strBuffer.append("(" + poolName + ":IaaS:service {name:'" +
			 * poolName + "', type:'pool', array_vms:'" + strAllVms +
			 * "', app_id:'" + appId + "'})\n");
			 */

			index++;
		} // end while

		// (containerTSM:PaaS:service {name:'containerTSM', type:'docker_swarm',
		// allocationStrategy: 'custom'}),
		// (poolTSM)-[:hosts {name:'poolTSM',
		// manager:'CPUIntensiveUbuntuUKVM'}]->(containerTSM),
		// *********I can�t get type from the xmi file !! It doen�t appear
		try {
			expr = xpath.compile("camelDsl/deploymentModels/containers[1]");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Node containerNode = null;
		try {
			containerNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String containerName = "";
		String allocationStrategy = "";
		String strPoolHost = "";
		String strPoolName = "";
		String strPoolVMHost = "";
		String strPoolVMName = "";
		String requiredHosts = "";
		String requiredVM = "";
		String requiredHost = "";
		String strCountryId = "";
		String strMinCore = "";
		String strMinRAM = "";

		if ((containerNode != null) && (containerNode.getAttributes() != null)
				&& (containerNode.getAttributes().getNamedItem("name") != null)
				&& (containerNode.getAttributes().getNamedItem("name").getTextContent() != null)) {
			containerName = containerNode.getAttributes().getNamedItem("name").getTextContent();
			allocationStrategy = containerNode.getAttributes().getNamedItem("allocationStrategyEnum").getTextContent();
			strPoolHost = containerNode.getAttributes().getNamedItem("requiredPoolHost").getTextContent();
			strPoolVMHost = containerNode.getAttributes().getNamedItem("requiredPoolVMHost").getTextContent();
			strBuffer.append("(" + containerName + ":PaaS:service {name:'" + containerName
					+ "', type:'docker_swarm', allocationStrategy: '" + allocationStrategy + "', app_id:'" + appId
					+ "'})\n");

			// index++;
			String poolHostXPathExpression = getXPathExpression(strPoolHost);
			try {
				expr = xpath.compile(poolHostXPathExpression);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Node poolHostNode = null;
			try {
				poolHostNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
				if ((poolHostNode != null) && (poolHostNode.getAttributes() != null)
						&& (poolHostNode.getAttributes().getNamedItem("name") != null)
						&& (poolHostNode.getAttributes().getNamedItem("name").getTextContent() != null)) {
					strPoolName = poolHostNode.getAttributes().getNamedItem("name").getTextContent();
					requiredHosts = poolHostNode.getAttributes().getNamedItem("requiredHosts").getTextContent();
					// poolTSM
				}

			} catch (XPathExpressionException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			StringTokenizer st = new StringTokenizer(requiredHosts, " ");

			while (st.hasMoreTokens()) {
				requiredHost = st.nextToken();
				requiredVM = requiredHost.substring(0, requiredHost.lastIndexOf("/"));
				String requiredVMXPathExpression = getXPathExpression(requiredVM);
				String requiredHostXPathExpression = getXPathExpression(requiredHost);
				// requiredHostName
				try {
					expr = xpath.compile(requiredHostXPathExpression);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Node requiredHostNode = null;

				try {
					requiredHostNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
				} catch (XPathExpressionException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				// CPUIntensiveUbuntuUKHost
				if ((requiredHostNode != null) && (requiredHostNode.getAttributes() != null)
						&& (requiredHostNode.getAttributes().getNamedItem("name") != null)
						&& (requiredHostNode.getAttributes().getNamedItem("name").getTextContent() != null)) {
					requiredHostName = requiredHostNode.getAttributes().getNamedItem("name").getTextContent();
				}

				try {
					expr = xpath.compile(requiredVMXPathExpression);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Node vmNode = null;
				try {
					vmNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
					// CPUIntensiveUbuntuUKHost
					if ((vmNode != null) && (vmNode.getAttributes() != null)
							&& (vmNode.getAttributes().getNamedItem("name") != null)
							&& (vmNode.getAttributes().getNamedItem("name").getTextContent() != null)) {
						vmName = vmNode.getAttributes().getNamedItem("name").getTextContent();
						strVmRequirementSet = vmNode.getAttributes().getNamedItem("vmRequirementSet").getTextContent();
						String vmRequirementSerXPathExpression = getXPathExpression(strVmRequirementSet);
						// get location

						try {
							expr = xpath.compile(vmRequirementSerXPathExpression);
						} catch (XPathExpressionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Node vmRequirementSetsNode = null;
						try {
							vmRequirementSetsNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
						} catch (XPathExpressionException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						if ((vmRequirementSetsNode != null) && (vmRequirementSetsNode.getAttributes() != null)
								&& (vmRequirementSetsNode.getAttributes().getNamedItem("locationRequirement") != null)
								&& (vmRequirementSetsNode.getAttributes()
										.getNamedItem("quantitativeHardwareRequirement") != null)) {
							strLocationRequirement = vmRequirementSetsNode.getAttributes()
									.getNamedItem("locationRequirement").getTextContent();
							strQuantitativeHardwareRequirement = vmRequirementSetsNode.getAttributes()
									.getNamedItem("quantitativeHardwareRequirement").getTextContent();
						}
						String locationRequirementXPathExpression = getXPathExpression(strLocationRequirement);
						try {
							expr = xpath.compile(locationRequirementXPathExpression);
						} catch (XPathExpressionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Node requirementsNode = null;
						try {
							requirementsNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
						} catch (XPathExpressionException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						String strRequirementLocations = requirementsNode.getAttributes().getNamedItem("locations")
								.getTextContent();
						String strRequirementLocationsXPathExpression = getXPathExpression(strRequirementLocations);

						try {
							expr = xpath.compile(strRequirementLocationsXPathExpression);
						} catch (XPathExpressionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Node countriesNode = null;
						try {
							countriesNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
						} catch (XPathExpressionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						strCountryId = "";
						if ((countriesNode != null) && (countriesNode.getAttributes() != null)
								&& (countriesNode.getAttributes().getNamedItem("id") != null)) {
							strCountryId = countriesNode.getAttributes().getNamedItem("id").getTextContent();
						}
						// GEV OCT
						String quantitativeHardwareRequirementXPathExpression = getXPathExpression(
								strQuantitativeHardwareRequirement);
						try {
							expr = xpath.compile(quantitativeHardwareRequirementXPathExpression);
						} catch (XPathExpressionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Node quantitativeHardwareRequirementNode = null;
						try {
							quantitativeHardwareRequirementNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
						} catch (XPathExpressionException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						if ((quantitativeHardwareRequirementNode != null)
								&& (quantitativeHardwareRequirementNode.getAttributes() != null)) {
							if (quantitativeHardwareRequirementNode.getAttributes().getNamedItem("minCores") != null)
								strMinCore = quantitativeHardwareRequirementNode.getAttributes()
										.getNamedItem("minCores").getTextContent();
							if (quantitativeHardwareRequirementNode.getAttributes().getNamedItem("minRAM") != null)
								strMinRAM = quantitativeHardwareRequirementNode.getAttributes().getNamedItem("minRAM")
										.getTextContent();
						}

					}

				} catch (XPathExpressionException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				strBuffer.append("(" + vmName + ":IaaS:service {name:'" + vmName + "', location:'" + strCountryId
						+ "', app_id:'" + appId + "', hardware_core:'" + strMinCore + "', hardware_ram:'" + strMinRAM
						+ "'})\n");
				// change for CUSTOM ALLOCATION
				/*
				 * strBuffer.append("(" + strPoolName + ")-[:hosts {name:'" +
				 * strPoolName + "', manager:'" + strPoolVMName + "'}]->(" +
				 * containerName + ")\n");
				 */
			} // END if ((containerNode != null)
			if ((vmName != null) && (vmName != "") && (containerName != null) && (containerName != "")
					&& (requiredHostName != null) && (requiredHostName != ""))
				strBuffer.append(
						"(" + vmName + ")-[:hosts {name:'" + requiredHostName + "'}]->(" + containerName + ")\n");

		}

		try {
			expr = xpath.compile("camelDsl/deploymentModels/internalComponents");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String requiredCommunicationName = "";

		try {
			internalComponentsNodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		lenghtInternalComponentNodeList = internalComponentsNodeList.getLength();
		Node internalComponent = null;
		NodeList internalComponentChildNodes = null;
		int internalComponentChildNodesLength;

		for (i = 0; i < lenghtInternalComponentNodeList; i++) {
			internalComponent = internalComponentsNodeList.item(i);
			strComponentType = "";
			componentName = internalComponent.getAttributes().getNamedItem("name").getTextContent();

			internalComponentChildNodes = internalComponent.getChildNodes();
			internalComponentChildNodesLength = internalComponentChildNodes.getLength();
			for (int k = 0; k < internalComponentChildNodesLength; k++) {
				Node itemk = internalComponentChildNodes.item(k);

				// new
				if (itemk.getNodeName().equalsIgnoreCase("requiredHost")) {
					requiredHostName = itemk.getAttributes().getNamedItem("name").getTextContent();
					// StorageIntensiveUbuntuFinlandHostReq
					requiredHostName = requiredHostName.replaceAll("Req", "");

					// requiredHostName =
					// requiredCommunicationName.replaceAll("Req", "");

					try {
						expr = xpath.compile(
								"camelDsl/deploymentModels/vms/providedHosts[@name='" + requiredHostName + "']");
					} catch (XPathExpressionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					Node providedHostNode = null;
					try {
						providedHostNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
						if (providedHostNode != null)
							vmName = providedHostNode.getParentNode().getAttributes().getNamedItem("name")
									.getTextContent();
					} catch (XPathExpressionException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					if ((vmName != null) && (vmName != "") && (containerName != null) && (containerName != "")
							&& (requiredHostName != null) && (requiredHostName != "") && (componentName != null)
							&& (componentName != ""))
						strBuffer.append("(" + containerName + ")-[:hosts {name:'" + requiredHostName + "'}]->("
								+ componentName + ")\n");

				} // end if

			} // end for

		} // end for

		// *************************communications
		NodeList communicationsNodeList = null;
		String strRequiredCommunication = "";
		String strProvidedCommunication = "";
		try {
			expr = xpath.compile("camelDsl/deploymentModels/communications");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			communicationsNodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		int lenghtCommunicationsNodeList = 0;
		if (communicationsNodeList != null) {
			lenghtCommunicationsNodeList = communicationsNodeList.getLength();
		}
		Node communicationNode = null;
		String internalComponentName = "";
		String usedInternalComponentName = "";

		for (i = 0; i < lenghtCommunicationsNodeList; i++) {
			communicationNode = communicationsNodeList.item(i);
			if ((communicationNode != null) && (communicationNode.getAttributes() != null)) {
				if (communicationNode.getAttributes().getNamedItem("requiredCommunication") != null)
					strRequiredCommunication = communicationNode.getAttributes().getNamedItem("requiredCommunication")
							.getTextContent();
				if (communicationNode.getAttributes().getNamedItem("providedCommunication") != null)
					strProvidedCommunication = communicationNode.getAttributes().getNamedItem("providedCommunication")
							.getTextContent();
			}
			String strRequiredCommunicationXPathExpression = getXPathExpression(strRequiredCommunication);
			String strProvidedCommunicationXPathExpression = getXPathExpression(strProvidedCommunication);
			// to get internalComponentName
			try {
				expr = xpath.compile(strRequiredCommunicationXPathExpression);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Node requiredCommunicationNode = null;

			try {
				requiredCommunicationNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if ((requiredCommunicationNode != null) && (requiredCommunicationNode.getParentNode() != null)
					&& (requiredCommunicationNode.getParentNode().getAttributes() != null)
					&& (requiredCommunicationNode.getParentNode().getAttributes().getNamedItem("name") != null)) {
				internalComponentName = requiredCommunicationNode.getParentNode().getAttributes().getNamedItem("name")
						.getTextContent();
			}

			// to get usedInternalComponentName
			try {
				expr = xpath.compile(strProvidedCommunicationXPathExpression);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Node providedCommunicationNode = null;

			try {
				providedCommunicationNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if ((providedCommunicationNode != null) && (providedCommunicationNode.getParentNode() != null)
					&& (providedCommunicationNode.getParentNode().getAttributes() != null)
					&& (providedCommunicationNode.getParentNode().getAttributes().getNamedItem("name") != null)) {
				usedInternalComponentName = providedCommunicationNode.getParentNode().getAttributes()
						.getNamedItem("name").getTextContent();
			}

			strBuffer.append(
					"(" + internalComponentName + ")-[:uses {name:'uses'}]->(" + usedInternalComponentName + ")\n");
		}

		NodeList capabilityLinksNodeList = null;
		String strRequiredMUSACapability = "";
		String strProvidedMUSACapability = "";
		try {
			expr = xpath.compile("camelDsl/deploymentModels/capabilityLinks");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			capabilityLinksNodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int lenghtCapabilityLinksNodeList = 0;
		if (capabilityLinksNodeList != null) {
			lenghtCapabilityLinksNodeList = capabilityLinksNodeList.getLength();
		}
		Node capabilityLinksNode = null;
		internalComponentName = "";
		usedInternalComponentName = "";

		for (i = 0; i < lenghtCapabilityLinksNodeList; i++) {
			capabilityLinksNode = capabilityLinksNodeList.item(i);
			if (capabilityLinksNode.getAttributes() != null) {
				if (capabilityLinksNode.getAttributes().getNamedItem("requiredMUSACapability") != null)
					strRequiredMUSACapability = capabilityLinksNode.getAttributes()
							.getNamedItem("requiredMUSACapability").getTextContent();
				if (capabilityLinksNode.getAttributes().getNamedItem("providedMUSACapability") != null)
					strProvidedMUSACapability = capabilityLinksNode.getAttributes()
							.getNamedItem("providedMUSACapability").getTextContent();
			}
			String strRequiredMUSACapabilityXPathExpression = getXPathExpression(strRequiredMUSACapability);
			String strProvidedMUSACapabilityXPathExpression = getXPathExpression(strProvidedMUSACapability);
			// to get internalComponentName
			try {
				expr = xpath.compile(strRequiredMUSACapabilityXPathExpression);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Node requiredMUSACapabilitiesNode = null;

			try {
				requiredMUSACapabilitiesNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if ((requiredMUSACapabilitiesNode != null) && (requiredMUSACapabilitiesNode.getParentNode() != null)
					&& (requiredMUSACapabilitiesNode.getParentNode().getAttributes() != null)
					&& (requiredMUSACapabilitiesNode.getParentNode().getAttributes().getNamedItem("name") != null)) {
				internalComponentName = requiredMUSACapabilitiesNode.getParentNode().getAttributes()
						.getNamedItem("name").getTextContent();
			}

			// to get usedInternalComponentName
			try {
				expr = xpath.compile(strProvidedMUSACapabilityXPathExpression);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Node providedMUSACapabilitiesNode = null;

			try {
				providedMUSACapabilitiesNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if ((providedMUSACapabilitiesNode != null) && (providedMUSACapabilitiesNode.getParentNode() != null)
					&& (providedMUSACapabilitiesNode.getParentNode().getAttributes() != null)
					&& (providedMUSACapabilitiesNode.getParentNode().getAttributes().getNamedItem("name") != null)) {
				usedInternalComponentName = providedMUSACapabilitiesNode.getParentNode().getAttributes()
						.getNamedItem("name").getTextContent();
			}

			strBuffer.append(
					"(" + internalComponentName + ")-[:uses {name:'uses'}]->(" + usedInternalComponentName + ")\n");
		}

		// ************************* hostings
		String poolProviderHost;
		try {
			expr = xpath.compile("camelDsl/deploymentModels/hostings");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		NodeList hostingsNodeList = null;
		try {
			hostingsNodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		lenght = hostingsNodeList.getLength();
		index = 0;
		int k = 0;
		while (index < lenght) {
			strPoolName = "";
			Node hostingNode = hostingsNodeList.item(index);
			if (hostingNode.getAttributes().getNamedItem("poolProvidedHost") != null) {
				poolProviderHost = hostingNode.getAttributes().getNamedItem("poolProvidedHost").getTextContent();
				StringTokenizer tokens = new StringTokenizer(poolProviderHost, " ");
				// k = 0;
				while (tokens.hasMoreTokens()) {
					String strpoolsXPathExpression = getXPathExpression(tokens.nextToken());
					try {
						expr = xpath.compile(strpoolsXPathExpression);
					} catch (XPathExpressionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					Node providedHostNode = null;
					try {
						providedHostNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
						strPoolName = providedHostNode.getParentNode().getAttributes().getNamedItem("name")
								.getTextContent();
					} catch (XPathExpressionException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} // end while
				requiredHost = hostingNode.getAttributes().getNamedItem("requiredHost").getTextContent();
				tokens = new StringTokenizer(requiredHost, " ");

				while (tokens.hasMoreTokens()) {
					String internalComponentXPathExpression = getXPathExpression(tokens.nextToken());
					try {
						expr = xpath.compile(internalComponentXPathExpression);
					} catch (XPathExpressionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					Node requiredHostHostNode = null;
					try {
						requiredHostHostNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
						strContainerName = requiredHostHostNode.getParentNode().getAttributes().getNamedItem("name")
								.getTextContent();
					} catch (XPathExpressionException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} // end while
					// change for CUSTOM ALLOCATION
				/*
				 * strBuffer.append( "(" + strPoolName + ")-[:hosts {name:'" +
				 * strPoolName + "'}]->(" + strContainerName + ")\n");
				 */
			} // end if

			// (poolTSM)-[:hosts {name:'poolTSM'}]->(JourneyPlanner)
			index++;
		} // end while
			// ******************************

		returnValue = strBuffer.toString();
		return returnValue;
	}

	/**
	 * Returns the allocation strategy value of the container
	 * 
	 * @param doc
	 * @return
	 */
	private static String getAllocationValue(Document doc) {
		// xpath inizialitation
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		XPathExpression expr = null;

		try {
			expr = xpath.compile("camelDsl/deploymentModels/containers[1]/@allocationStrategyEnum");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String allocationStrategy = null;
		try {
			allocationStrategy = (String) expr.evaluate(doc, XPathConstants.STRING);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return allocationStrategy;
	}

	/**
	 * Checks if the model contains a container
	 * 
	 * @param doc
	 * @return
	 */
	private static boolean checkIfContainerExists(Document doc) {
		// xpath inizialitation
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		XPathExpression expr = null;

		try {
			expr = xpath.compile("camelDsl/deploymentModels/containers");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Object result = null;
		try {
			result = expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		NodeList nodes = (NodeList) result;
		if (nodes.getLength() == 0)
			return false;
		else
			return true;
	}

	/**
	 * Returns the xpath expression given the value obtained from the xmi file
	 * 
	 * @param xmiExpression
	 * @return
	 */
	private static String getXPathExpression(String xmiExpression) {
		// This is the format of the source:
		// //@deploymentModels.0/@vmRequirementSets.0"
		// This is the format of the target:
		// //deploymentModels[1]/vmRequirementSets[1]"

		// 1 transformation
		String strTransformedExpression = xmiExpression;
		Pattern digitPattern;
		Matcher matcher;
		StringBuffer result;
		/*
		 * it is neccesary to add 1 to any ordering value for xpath Pattern
		 * digitPattern = Pattern.compile("(\\d)"); // EDIT: Increment each //
		 * digit.
		 * 
		 * Matcher matcher = digitPattern.matcher(xmiExpression); StringBuffer
		 * result = new StringBuffer(); while (matcher.find()) {
		 * matcher.appendReplacement(result,
		 * String.valueOf(Integer.parseInt(matcher.group(1)) + 1)); }
		 * matcher.appendTail(result); strTransformedExpression =
		 * result.toString();
		 */
		// 2 transformation
		// the maximum is 3 correlative digits
		digitPattern = Pattern.compile("(\\.)(\\d)(\\d)(\\d)");
		matcher = digitPattern.matcher(strTransformedExpression);
		result = new StringBuffer();
		while (matcher.find()) {
			matcher.appendReplacement(result,
					"[" + String.valueOf(Integer.parseInt(matcher.group(2) + matcher.group(3) + matcher.group(4)) + 1)
							+ "]");
		}
		matcher.appendTail(result);
		strTransformedExpression = result.toString();

		digitPattern = Pattern.compile("(\\.)(\\d)(\\d)");
		matcher = digitPattern.matcher(strTransformedExpression);
		result = new StringBuffer();
		while (matcher.find()) {
			matcher.appendReplacement(result,
					"[" + String.valueOf(Integer.parseInt(matcher.group(2) + matcher.group(3)) + 1) + "]");
		}
		matcher.appendTail(result);
		strTransformedExpression = result.toString();

		digitPattern = Pattern.compile("(\\.)(\\d)");
		matcher = digitPattern.matcher(strTransformedExpression);
		result = new StringBuffer();
		while (matcher.find()) {
			matcher.appendReplacement(result, "[" + String.valueOf(Integer.parseInt(matcher.group(2)) + 1) + "]");
		}
		matcher.appendTail(result);
		strTransformedExpression = result.toString();

		// 2 transformation
		strTransformedExpression = strTransformedExpression.replace("@", "");

		return strTransformedExpression;
	}

	/**
	 * Relations are put after node declarations -> it is a requirement of neo4j
	 * tool to work
	 * 
	 * @param strMACMModel
	 * @return
	 */
	private static String getOrderedMACMModel(String strMACMModel) {
		StringBuffer strBufferResult = new StringBuffer("");
		String[] aux = strMACMModel.split("\n");
		for (int i = 0; i < aux.length; i++) {
			if ((aux[i]).contains("IaaS:service")) {
				strBufferResult.append(aux[i] + ",\n");
				aux[i] = "";
			}
		}
		for (int i = 0; i < aux.length; i++) {
			if ((aux[i]).contains("PaaS:service")) {
				strBufferResult.append(aux[i] + ",\n");
				aux[i] = "";
			}
		}
		for (int i = 0; i < aux.length; i++) {
			if ((aux[i]).contains("SaaS:service")) {
				strBufferResult.append(aux[i] + ",\n");
				aux[i] = "";
			}
		}
		for (int i = 0; i < aux.length; i++) {
			if ((aux[i]) != "") {
				strBufferResult.append(aux[i] + ",\n");
			}
		}
		return strBufferResult.toString();
	}

	/**
	 * This method returns the end index of a the occurrence of given pattern
	 * inside a string
	 * 
	 * @param text
	 * @param regex
	 * @return
	 */
	// TODO move to utils project when possible
	public static int getMatchedEndIndex(String text, String regex) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(text);
		int index = 0;
		// Check all occurrences
		while (matcher.find()) {
			index = matcher.end();
		}
		return index;
	}

	/**
	 * This method returns the start index of a the occurrence of given pattern
	 * inside a string
	 * 
	 * @param text
	 * @param regex
	 * @return
	 */
	// TODO move to utils project when possible
	public static int getMatchedStartIndex(String text, String regex) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(text);
		int index = 0;
		// Check all occurrences
		while (matcher.find()) {
			index = matcher.start();
		}
		return index;
	}

	/**
	 * Returns the model in MACM format in those cases where there is not a
	 * container in the model
	 * 
	 * @param camelInXMIFormat
	 * @param appId
	 * @return
	 */
	private String getCamelInMACMFormat_No_Container(String camelInXMIFormat, String appId) {
		// xpath inizialitation
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document doc = null;
		String content = "";
		String strMACMModel;
		Long mcAppID = new Long(appId);
		// "camelDsl:CamelPlusModel" element name gives problems
		content = camelInXMIFormat.replace("camelDsl:CamelPlusModel", "camelDsl");

		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			doc = builder.parse(new InputSource(new StringReader(content)));
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		XPathExpression expr = null;

		// IAAS

		try {
			expr = xpath.compile("camelDsl/deploymentModels/vms");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		StringBuffer strBuffer = new StringBuffer("");

		NodeList vmsNodeList = null;
		try {
			vmsNodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int lenght = vmsNodeList.getLength();
		int index = 0;
		String vmName = "";
		String strVmRequirementSet = "";
		String strLocationRequirement = "";
		String strQuantitativeHardwareRequirement = "";

		while (index < lenght) {
			Node vmNode = vmsNodeList.item(index);
			vmName = vmNode.getAttributes().getNamedItem("name").getTextContent();
			strVmRequirementSet = vmNode.getAttributes().getNamedItem("vmRequirementSet").getTextContent();
			String vmRequirementSerXPathExpression = getXPathExpression(strVmRequirementSet);
			// get location
			/*
			 * <vms name="JourneyPlanner"
			 * vmRequirementSet="//@deploymentModels.0/@vmRequirementSets.0">
			 * <vmRequirementSets name="JourneyPlannerHostRS"
			 * locationRequirement="//@requirementModels.0/@requirements.6"
			 * quantitativeHardwareRequirement=
			 * "//@requirementModels.0/@requirements.0"
			 * osOrImageRequirement="//@requirementModels.0/@requirements.4"/>
			 * <requirements xsi:type="requirement:LocationRequirement"
			 * name="GermanyReq" locations="//@locationModels.0/@countries.0"/>
			 * <countries id="DE" name="Germany"/>
			 */
			try {
				expr = xpath.compile(vmRequirementSerXPathExpression);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Node vmRequirementSetsNode = null;
			try {
				vmRequirementSetsNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
			} catch (XPathExpressionException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if ((vmRequirementSetsNode != null) && (vmRequirementSetsNode.getAttributes() != null)
					&& (vmRequirementSetsNode.getAttributes().getNamedItem("locationRequirement") != null)) {
				strLocationRequirement = vmRequirementSetsNode.getAttributes().getNamedItem("locationRequirement")
						.getTextContent();
				strQuantitativeHardwareRequirement = vmRequirementSetsNode.getAttributes()
						.getNamedItem("quantitativeHardwareRequirement").getTextContent();
			}
			String locationRequirementXPathExpression = getXPathExpression(strLocationRequirement);
			try {
				expr = xpath.compile(locationRequirementXPathExpression);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Node requirementsNode = null;
			try {
				requirementsNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
			} catch (XPathExpressionException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			/*
			 * String strRequirementLocations = ""; if ((requirementsNode !=
			 * null) && (requirementsNode.getAttributes() != null) &&
			 * (requirementsNode.getAttributes().getNamedItem("locations") !=
			 * null)) { strRequirementLocations =
			 * vmRequirementSetsNode.getAttributes().getNamedItem(
			 * "locationRequirement") .getTextContent(); }
			 */
			String strRequirementLocations = "";
			String strRequirementLocationsXPathExpression = "";
			String strCountryId = "";
			String strMinCore = "";
			String strMinRAM = "";
			if ((requirementsNode != null) && (requirementsNode.getAttributes() != null)
					&& (requirementsNode.getAttributes().getNamedItem("locations") != null)) {
				strRequirementLocations = requirementsNode.getAttributes().getNamedItem("locations").getTextContent();
				strRequirementLocationsXPathExpression = getXPathExpression(strRequirementLocations);

				try {
					expr = xpath.compile(strRequirementLocationsXPathExpression);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Node countriesNode = null;
				try {
					countriesNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if ((countriesNode != null) && (countriesNode.getAttributes() != null)
						&& (countriesNode.getAttributes().getNamedItem("id") != null)) {
					strCountryId = countriesNode.getAttributes().getNamedItem("id").getTextContent();
				}
			}

			// GEV OCT
			String quantitativeHardwareRequirementXPathExpression = getXPathExpression(
					strQuantitativeHardwareRequirement);
			try {
				expr = xpath.compile(quantitativeHardwareRequirementXPathExpression);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Node quantitativeHardwareRequirementNode = null;
			try {
				quantitativeHardwareRequirementNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
			} catch (XPathExpressionException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if ((quantitativeHardwareRequirementNode != null)
					&& (quantitativeHardwareRequirementNode.getAttributes() != null)) {
				if (quantitativeHardwareRequirementNode.getAttributes().getNamedItem("minCores") != null)
					strMinCore = quantitativeHardwareRequirementNode.getAttributes().getNamedItem("minCores")
							.getTextContent();
				if (quantitativeHardwareRequirementNode.getAttributes().getNamedItem("minRAM") != null)
					strMinRAM = quantitativeHardwareRequirementNode.getAttributes().getNamedItem("minRAM")
							.getTextContent();
			}

			strBuffer.append(
					"(" + vmName + ":IaaS:service {name:'" + vmName + "', location:'" + strCountryId + "', app_id:'"
							+ appId + "', hardware_core:'" + strMinCore + "', hardware_ram:'" + strMinRAM + "'})\n");

			index++;
		} // end while

		// SAAS
		try {
			expr = xpath.compile("camelDsl/deploymentModels/internalComponents");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		NodeList internalComponentsNodeList = null;
		int lenghtInternalComponentNodeList;
		int i;
		String componentName = null;
		String strComponentType = null;
		String strComponentTypeXPath = null;
		String requiredHostName = null;
		String strComponentName = null;
		String strMUSASecurityCapability = null;
		String strSecurityCapability = null;
		String strRequiredContainer = null;
		String strRequiredContainerXPath = null;
		String strContainerName = null;

		try {
			internalComponentsNodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		lenghtInternalComponentNodeList = internalComponentsNodeList.getLength();
		i = 0;
		componentName = "";
		strComponentType = "";
		strComponentTypeXPath = "";
		requiredHostName = "";
		strComponentName = "";
		ManageMCApp base = new ManageMCApp();
		HashMap<Object, Object> componentsIdsByApplication = base.getComponentsIdsByApplication(mcAppID);
		HashMap<Object, Object> componentsToBeInserted = new HashMap<>();
		int componentId = 0;
		while (i < lenghtInternalComponentNodeList) {
			componentName = "";
			strComponentType = "";
			strComponentTypeXPath = "";
			requiredHostName = "";
			strComponentName = "";
			strMUSASecurityCapability = "";
			strSecurityCapability = "";
			strRequiredContainer = "";
			strRequiredContainerXPath = "";
			strContainerName = "";

			Node internalComponent = internalComponentsNodeList.item(i);
			strComponentType = "";

			componentName = internalComponent.getAttributes().getNamedItem("name").getTextContent();
			if (internalComponent.getAttributes().getNamedItem("type") != null) { // 1
				strComponentType = internalComponent.getAttributes().getNamedItem("type").getTextContent();
				strComponentTypeXPath = getXPathExpression(strComponentType);
				try {
					expr = xpath.compile(strComponentTypeXPath);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Node musaComponentTypeNode = null;
				try {
					musaComponentTypeNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				strComponentName = musaComponentTypeNode.getAttributes().getNamedItem("name").getTextContent();
			} // 1

			if (internalComponent.getAttributes().getNamedItem("requiredContainer") != null) { // 6
				strRequiredContainer = internalComponent.getAttributes().getNamedItem("requiredContainer")
						.getTextContent();
				strRequiredContainerXPath = getXPathExpression(strRequiredContainer);
				try {
					expr = xpath.compile(strRequiredContainerXPath);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Node containerNode = null;
				try {
					containerNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if ((containerNode != null) && (containerNode.getAttributes() != null)
						&& (containerNode.getAttributes().getNamedItem("name") != null)
						&& (containerNode.getAttributes().getNamedItem("name").getTextContent() != null)) {
					strContainerName = containerNode.getAttributes().getNamedItem("name").getTextContent();
				}
			} // 6

			// set the information related to the provided security
			// (provMUSAsecurityCapability parameter)
			// <providedMUSACapabilities name="TSMEngineCap"
			// provMUSAsecurityCapability="//@securityModels.0/@MUSAsecurityCapabilities.1"/>
			// <MUSAsecurityCapabilities name="CAP2"
			// MUSAsecurityControls="//@securityModels.1/@MUSAsecurityControls.72
			// //@securityModels.1/@MUSAsecurityControls.191
			// //@securityModels.1/@MUSAsecurityControls.96"/>
			// <MUSAsecurityControls id="AC-1".............>
			try {
				// this expression selects the capabilities containing the
				// provMUSAsecurityCapability parameter
				expr = xpath.compile("camelDsl/deploymentModels/internalComponents[@name='" + componentName
						+ "']/providedMUSACapabilities[@provMUSAsecurityCapability]");

				Node providedMusaSecurityCapabilities = (Node) expr.evaluate(doc, XPathConstants.NODE);
				if (providedMusaSecurityCapabilities != null) {
					strMUSASecurityCapability = providedMusaSecurityCapabilities.getAttributes()
							.getNamedItem("provMUSAsecurityCapability").getTextContent();
				}
			} catch (XPathExpressionException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (strMUSASecurityCapability != "") {// 2

				strSecurityCapability = "";

				String musaSecurityCapabilityXPathExpression = getXPathExpression(strMUSASecurityCapability);

				try {
					expr = xpath.compile(musaSecurityCapabilityXPathExpression);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Node musaSecurityCapabilityNode = null;
				try {
					musaSecurityCapabilityNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
				} catch (XPathExpressionException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				String strMUSASecurityControls = "";
				StringBuffer strBufferSecurityControls = new StringBuffer("");

				if ((musaSecurityCapabilityNode != null) && (musaSecurityCapabilityNode.getAttributes() != null)
						&& (musaSecurityCapabilityNode.getAttributes().getNamedItem("MUSAsecurityControls") != null)
						&& (musaSecurityCapabilityNode.getAttributes().getNamedItem("MUSAsecurityControls")
								.getTextContent() != null)) {

					strMUSASecurityControls = musaSecurityCapabilityNode.getAttributes()
							.getNamedItem("MUSAsecurityControls").getTextContent();

					// there coulb be more than 1 controls
					String[] strMUSASecurityControlsSplitted = strMUSASecurityControls.split(" ");
					for (int j = 0; j < strMUSASecurityControlsSplitted.length; j++) {
						String strSecurityControl = strMUSASecurityControlsSplitted[j];
						String strSecurityControlXPathExpression = getXPathExpression(strSecurityControl);
						try {
							expr = xpath.compile(strSecurityControlXPathExpression);
						} catch (XPathExpressionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Node musaSecurityControlNode = null;
						try {
							musaSecurityControlNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
						} catch (XPathExpressionException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						if (musaSecurityControlNode != null) {
							strBufferSecurityControls.append(
									musaSecurityControlNode.getAttributes().getNamedItem("id").getTextContent());
							if (j != strMUSASecurityControlsSplitted.length - 1) {
								strBufferSecurityControls.append(", ");
							}
						}
						strSecurityCapability = strBufferSecurityControls.toString();
					} // end for

				} // end if no security controls
			} // 2
				// //logic included in order to set the value of the component
				// id

			if (componentsIdsByApplication.containsKey(componentName)) {
				componentId = new Integer((String) componentsIdsByApplication.get(componentName)).intValue();
			} else {
				componentId = getComponentIdMaxValue(componentsIdsByApplication) + 1;
				componentsToBeInserted.put(componentName, new Integer(componentId).toString());
				componentsIdsByApplication.put(componentName, new Integer(componentId).toString());
			}
			strBuffer.append("(" + componentName + ":SaaS:service {name:'" + componentName + "',type:'"
					+ strComponentName + "', seccap_provided:'" + strSecurityCapability + "', app_id:'" + appId
					+ "', component_id:'" + componentId + "'})\n");
			if (strContainerName != "") {
				strBuffer.append("(" + strContainerName + ")-[:hosts {name:'" + strContainerName + "'}]->("
						+ componentName + ")\n");
			}
			// "(webapp1:SaaS:service {name:'webapp1',type:'webapp'})";
			// "(db1:SaaS:service {name:'db1', type:'db',
			// seccap_provided:�AC-11(1), AU-13(2)�})";
			i++;
		} // end while
			// GEV commented in nov cos it is done in
			// updateComponentsPerAppliction method
			// base.insertMissingComponentCodes(mcAppID,
			// componentsToBeInserted);
			// RELATIONS
		/*
		 * we consider that a component uses other component if the first one
		 * requires a communication (requiredCommunications parameter) offered
		 * by other component (providedCommunication parameter) example
		 * TSMEngine requires IDManagerPort
		 *****
		 * <internalComponents name="TSMEngine" type="//@musaComponentTypes.2"
		 * order="3"> <providedCommunications name="TSMEnginePort"
		 * portNumber="8185"/> <providedCommunications name="TSMEngineRESTPort"
		 * portNumber="443"/> <configurations name="TSMEngineConfigurationCHEF">
		 * <confManager name="C1" cookbook="'tut'" recipe="'musa_tsme'"/>
		 * </configurations> <requiredCommunications name="IDManagerPortReq"
		 * portNumber="3000" isMandatory="true"/> <requiredCommunications
		 * name="ConsumptionEstimatorPortReq" portNumber="9090"
		 * isMandatory="true"/> <requiredCommunications
		 * name="JourneyPlannerPortReq" portNumber="8085" isMandatory="true"/>
		 * <requiredCommunications name="DatabasePortReq" portNumber="3306"
		 * isMandatory="true"/> <requiredHost
		 * name="CoreIntensiveUbuntuFinlandHostReq"/> <requiredMUSACapabilities
		 * name="MUSAAgentACCapReq" reqMUSAsecurityCapability=
		 * "//@securityModels.0/@MUSAsecurityCapabilities.0"/>
		 * <providedMUSACapabilities name="TSMEngineCap"
		 * provMUSAsecurityCapability=
		 * "//@securityModels.0/@MUSAsecurityCapabilities.1"/>
		 * </internalComponents>
		 * 
		 * IDManagerPort is offered by IDMAM
		 ********
		 * <internalComponents name="IDMAM"> <providedCommunications
		 * name="IDManagerPort" portNumber="3000"/> <providedCommunications
		 * name="MongoDBPort" portNumber="27017"/> <configurations
		 * name="IDManagerManualConfiguration"> <confManager name="C1"
		 * cookbook="'tut'" recipe="'musa_idm'"/> </configurations>
		 * <requiredHost name="StorageIntensiveUbuntuFinlandHostReq"/>
		 * </internalComponents>
		 */

		try {
			expr = xpath.compile("camelDsl/deploymentModels/internalComponents");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String requiredCommunicationName = "";

		try {
			internalComponentsNodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		lenghtInternalComponentNodeList = internalComponentsNodeList.getLength();
		Node internalComponent = null;
		NodeList internalComponentChildNodes = null;
		int internalComponentChildNodesLength;

		for (i = 0; i < lenghtInternalComponentNodeList; i++) {
			internalComponent = internalComponentsNodeList.item(i);
			strComponentType = "";
			componentName = internalComponent.getAttributes().getNamedItem("name").getTextContent();

			internalComponentChildNodes = internalComponent.getChildNodes();
			internalComponentChildNodesLength = internalComponentChildNodes.getLength();
			for (int k = 0; k < internalComponentChildNodesLength; k++) {
				Node itemk = internalComponentChildNodes.item(k);

				vmName = "";

				// new
				// <internalComponents
				// <requiredHost name="TSMEngineHostReq"/>
				/*
				 * if (itemk.getNodeName().equalsIgnoreCase("requiredHost")) {
				 * requiredHostName =
				 * itemk.getAttributes().getNamedItem("name").getTextContent();
				 * // StorageIntensiveUbuntuFinlandHostReq requiredHostName =
				 * requiredHostName.replaceAll("Req", "");
				 * 
				 * // requiredHostName = //
				 * requiredCommunicationName.replaceAll("Req", "");
				 * 
				 * try { expr = xpath.compile(
				 * "camelDsl/deploymentModels/vms/providedHosts[@name='" +
				 * requiredHostName + "']"); } catch (XPathExpressionException
				 * e) { // TODO Auto-generated catch block e.printStackTrace();
				 * }
				 * 
				 * Node providedHostNode = null; try { providedHostNode = (Node)
				 * expr.evaluate(doc, XPathConstants.NODE); if (providedHostNode
				 * != null) vmName =
				 * providedHostNode.getParentNode().getAttributes().getNamedItem
				 * ("name") .getTextContent(); } catch (XPathExpressionException
				 * e1) { // TODO Auto-generated catch block
				 * e1.printStackTrace(); } if ((vmName != null) && (vmName !=
				 * "")) strBuffer.append("(" + vmName + ")-[:hosts {name:'" +
				 * requiredHostName + "'}]->(" + componentName + ")\n");
				 * 
				 * } // end if
				 */ if (itemk.getNodeName().equalsIgnoreCase("requiredHost")) {
					requiredHostName = itemk.getAttributes().getNamedItem("name").getTextContent();

					Node hostingNode = null;
					String providedHost = null;
					// check the hosting nodes till one of them has the
					// requiresHostName assigned

					try {
						expr = xpath.compile("camelDsl/deploymentModels/hostings");
					} catch (XPathExpressionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					NodeList hostingNodeList = null;
					int lenghtHostingNodeList;

					String strRequiredHost = "";
					String strRequiredHostXPathExpression = "";
					String strRequiredHostName = "";
					String strProvidedHost = "";
					String strProvidedHostXPathExpression = "";
					String strProvidedHostName = "";
					try {
						hostingNodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
					} catch (XPathExpressionException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					lenghtHostingNodeList = hostingNodeList.getLength();
					int h = 0;
					String hostingName = "";

					while (h < lenghtHostingNodeList) {
						hostingNode = hostingNodeList.item(h);
						hostingName = hostingNode.getAttributes().getNamedItem("name").getTextContent();
						strRequiredHost = hostingNode.getAttributes().getNamedItem("requiredHost").getTextContent();

						strRequiredHostXPathExpression = getXPathExpression(strRequiredHost);

						try {
							expr = xpath.compile(strRequiredHostXPathExpression);
						} catch (XPathExpressionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Node requiredHostNode = null;
						try {
							requiredHostNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
						} catch (XPathExpressionException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

						if (requiredHostNode != null) {
							if (requiredHostNode.getAttributes().getNamedItem("name") != null)
								strRequiredHostName = requiredHostNode.getAttributes().getNamedItem("name")
										.getTextContent();
							if (strRequiredHostName.equalsIgnoreCase(requiredHostName)) {
								// get provided host
								if (hostingNode.getAttributes().getNamedItem("providedHost") != null)
									strProvidedHost = hostingNode.getAttributes().getNamedItem("providedHost")
											.getTextContent();
								strProvidedHostXPathExpression = getXPathExpression(strProvidedHost);

								try {
									expr = xpath.compile(strProvidedHostXPathExpression);
								} catch (XPathExpressionException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								Node providedHostNode = null;
								try {
									providedHostNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
									if (providedHostNode != null) {
										if (providedHostNode.getParentNode().getAttributes()
												.getNamedItem("name") != null)
											vmName = providedHostNode.getParentNode().getAttributes()
													.getNamedItem("name").getTextContent();
										if (providedHostNode.getAttributes().getNamedItem("name") != null)
											requiredHostName = providedHostNode.getAttributes().getNamedItem("name")
													.getTextContent();
									}

									if ((vmName != null) && (vmName != ""))
										strBuffer.append("(" + vmName + ")-[:hosts {name:'" + requiredHostName
												+ "'}]->(" + componentName + ")\n");
								} catch (XPathExpressionException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
								break;
							} // end if
								// (strRequiredHostName.equalsIgnoreCase(requiredHostName)){

						} // end if (requiredHostNode != null) {
						h++;
					} // end while (h < lenghtHostingNodeList) {

				} // end if
					// (itemk.getNodeName().equalsIgnoreCase("requiredHost")) {

			} // end for

		} // end for

		// PAAS
		// (poolTSM:IaaS:service {name:'poolTSM', type:'pool',
		// array_vms:'CPUIntensiveUbuntuUKVM' 'CPUIntensiveUbuntuFinlandVM'}),
		try {
			expr = xpath.compile("camelDsl/deploymentModels/pools");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		NodeList poolsNodeList = null;
		try {
			poolsNodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		lenght = poolsNodeList.getLength();
		index = 0;
		int j = 0;
		String poolName;
		String strVms;
		String strAllVms;
		String strVmXPathExpression = getXPathExpression(strVmRequirementSet);

		while (index < lenght) {
			poolName = "";
			strVms = "";
			strAllVms = "";
			Node poolNode = poolsNodeList.item(index);
			poolName = poolNode.getAttributes().getNamedItem("name").getTextContent();
			strVms = poolNode.getAttributes().getNamedItem("requiredHosts").getTextContent();
			StringTokenizer tokens = new StringTokenizer(strVms, " ");
			j = 0;
			while (tokens.hasMoreTokens()) {
				strVmXPathExpression = getXPathExpression(tokens.nextToken());
				try {
					expr = xpath.compile(strVmXPathExpression);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Node vmProvidedHostNode = null;
				try {
					vmProvidedHostNode = (Node) expr.evaluate(doc, XPathConstants.NODE);

					if ((vmProvidedHostNode != null) && (vmProvidedHostNode.getAttributes() != null)
							&& (vmProvidedHostNode.getAttributes().getNamedItem("name") != null)
							&& (vmProvidedHostNode.getAttributes().getNamedItem("name").getTextContent() != null)) {
						if (j > 0)
							strAllVms += ", ";
						strAllVms += vmProvidedHostNode.getParentNode().getAttributes().getNamedItem("name")
								.getTextContent();
					}
				} catch (XPathExpressionException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				j++;
				// strLocationRequirement =
				// vmRequirementSetsNode.getAttributes().getNamedItem("locationRequirement").getTextContent();
			}

			strBuffer.append("(" + poolName + ":IaaS:service {name:'" + poolName + "', type:'pool', array_vms:'"
					+ strAllVms + "', app_id:'" + appId + "'})\n");

			index++;
		} // end while

		// *************************communications
		NodeList communicationsNodeList = null;
		String strRequiredCommunication = "";
		String strProvidedCommunication = "";
		try {
			expr = xpath.compile("camelDsl/deploymentModels/communications");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			communicationsNodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		int lenghtCommunicationsNodeList = 0;
		if (communicationsNodeList != null) {
			lenghtCommunicationsNodeList = communicationsNodeList.getLength();
		}
		Node communicationNode = null;
		String internalComponentName = "";
		String usedInternalComponentName = "";

		for (i = 0; i < lenghtCommunicationsNodeList; i++) {
			communicationNode = communicationsNodeList.item(i);
			if ((communicationNode != null) && (communicationNode.getAttributes() != null)) {
				if (communicationNode.getAttributes().getNamedItem("requiredCommunication") != null)
					strRequiredCommunication = communicationNode.getAttributes().getNamedItem("requiredCommunication")
							.getTextContent();
				if (communicationNode.getAttributes().getNamedItem("providedCommunication") != null)
					strProvidedCommunication = communicationNode.getAttributes().getNamedItem("providedCommunication")
							.getTextContent();
			}
			String strRequiredCommunicationXPathExpression = getXPathExpression(strRequiredCommunication);
			String strProvidedCommunicationXPathExpression = getXPathExpression(strProvidedCommunication);
			// to get internalComponentName
			try {
				expr = xpath.compile(strRequiredCommunicationXPathExpression);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Node requiredCommunicationNode = null;

			try {
				requiredCommunicationNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if ((requiredCommunicationNode != null) && (requiredCommunicationNode.getParentNode() != null)
					&& (requiredCommunicationNode.getParentNode().getAttributes() != null)
					&& (requiredCommunicationNode.getParentNode().getAttributes().getNamedItem("name") != null)) {
				internalComponentName = requiredCommunicationNode.getParentNode().getAttributes().getNamedItem("name")
						.getTextContent();
			}

			// to get usedInternalComponentName
			try {
				expr = xpath.compile(strProvidedCommunicationXPathExpression);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Node providedCommunicationNode = null;

			try {
				providedCommunicationNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if ((providedCommunicationNode != null) && (providedCommunicationNode.getParentNode() != null)
					&& (providedCommunicationNode.getParentNode().getAttributes() != null)
					&& (providedCommunicationNode.getParentNode().getAttributes().getNamedItem("name") != null)) {
				usedInternalComponentName = providedCommunicationNode.getParentNode().getAttributes()
						.getNamedItem("name").getTextContent();
			}

			strBuffer.append(
					"(" + internalComponentName + ")-[:uses {name:'uses'}]->(" + usedInternalComponentName + ")\n");
		}

		NodeList capabilityLinksNodeList = null;
		String strRequiredMUSACapability = "";
		String strProvidedMUSACapability = "";
		try {
			expr = xpath.compile("camelDsl/deploymentModels/capabilityLinks");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			capabilityLinksNodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int lenghtCapabilityLinksNodeList = 0;
		if (capabilityLinksNodeList != null) {
			lenghtCapabilityLinksNodeList = capabilityLinksNodeList.getLength();
		}
		Node capabilityLinksNode = null;
		internalComponentName = "";
		usedInternalComponentName = "";

		for (i = 0; i < lenghtCapabilityLinksNodeList; i++) {
			capabilityLinksNode = capabilityLinksNodeList.item(i);
			if (capabilityLinksNode.getAttributes() != null) {
				if (capabilityLinksNode.getAttributes().getNamedItem("requiredMUSACapability") != null)
					strRequiredMUSACapability = capabilityLinksNode.getAttributes()
							.getNamedItem("requiredMUSACapability").getTextContent();
				if (capabilityLinksNode.getAttributes().getNamedItem("providedMUSACapability") != null)
					strProvidedMUSACapability = capabilityLinksNode.getAttributes()
							.getNamedItem("providedMUSACapability").getTextContent();
			}
			String strRequiredMUSACapabilityXPathExpression = getXPathExpression(strRequiredMUSACapability);
			String strProvidedMUSACapabilityXPathExpression = getXPathExpression(strProvidedMUSACapability);
			// to get internalComponentName
			try {
				expr = xpath.compile(strRequiredMUSACapabilityXPathExpression);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Node requiredMUSACapabilitiesNode = null;

			try {
				requiredMUSACapabilitiesNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if ((requiredMUSACapabilitiesNode != null) && (requiredMUSACapabilitiesNode.getParentNode() != null)
					&& (requiredMUSACapabilitiesNode.getParentNode().getAttributes() != null)
					&& (requiredMUSACapabilitiesNode.getParentNode().getAttributes().getNamedItem("name") != null)) {
				internalComponentName = requiredMUSACapabilitiesNode.getParentNode().getAttributes()
						.getNamedItem("name").getTextContent();
			}

			// to get usedInternalComponentName
			try {
				expr = xpath.compile(strProvidedMUSACapabilityXPathExpression);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Node providedMUSACapabilitiesNode = null;

			try {
				providedMUSACapabilitiesNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if ((providedMUSACapabilitiesNode != null) && (providedMUSACapabilitiesNode.getParentNode() != null)
					&& (providedMUSACapabilitiesNode.getParentNode().getAttributes() != null)
					&& (providedMUSACapabilitiesNode.getParentNode().getAttributes().getNamedItem("name") != null)) {
				usedInternalComponentName = providedMUSACapabilitiesNode.getParentNode().getAttributes()
						.getNamedItem("name").getTextContent();
			}

			strBuffer.append(
					"(" + internalComponentName + ")-[:uses {name:'uses'}]->(" + usedInternalComponentName + ")\n");
		}

		// ************************* hostings
		String strPoolName;
		String poolProviderHost;
		String requiredHost;
		try {
			expr = xpath.compile("camelDsl/deploymentModels/hostings");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		NodeList hostingsNodeList = null;
		try {
			hostingsNodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		lenght = hostingsNodeList.getLength();
		index = 0;
		int k = 0;
		while (index < lenght) {
			strPoolName = "";
			Node hostingNode = hostingsNodeList.item(index);
			if (hostingNode.getAttributes().getNamedItem("poolProvidedHost") != null) {
				poolProviderHost = hostingNode.getAttributes().getNamedItem("poolProvidedHost").getTextContent();
				StringTokenizer tokens = new StringTokenizer(poolProviderHost, " ");
				// k = 0;
				while (tokens.hasMoreTokens()) {
					String strpoolsXPathExpression = getXPathExpression(tokens.nextToken());
					try {
						expr = xpath.compile(strpoolsXPathExpression);
					} catch (XPathExpressionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					Node providedHostNode = null;
					try {
						providedHostNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
						strPoolName = providedHostNode.getParentNode().getAttributes().getNamedItem("name")
								.getTextContent();
					} catch (XPathExpressionException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} // end while
				requiredHost = hostingNode.getAttributes().getNamedItem("requiredHost").getTextContent();
				tokens = new StringTokenizer(requiredHost, " ");

				while (tokens.hasMoreTokens()) {
					String internalComponentXPathExpression = getXPathExpression(tokens.nextToken());
					try {
						expr = xpath.compile(internalComponentXPathExpression);
					} catch (XPathExpressionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					Node requiredHostHostNode = null;
					try {
						requiredHostHostNode = (Node) expr.evaluate(doc, XPathConstants.NODE);
						strContainerName = requiredHostHostNode.getParentNode().getAttributes().getNamedItem("name")
								.getTextContent();
					} catch (XPathExpressionException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} // end while
				strBuffer.append(
						"(" + strPoolName + ")-[:hosts {name:'" + strPoolName + "'}]->(" + strContainerName + ")\n");
			} // end if

			// (poolTSM)-[:hosts {name:'poolTSM'}]->(JourneyPlanner)
			index++;
		} // end while
			// ******************************
		strMACMModel = strBuffer.toString();

		String strOrderedMACMModel = getOrderedMACMModel(strMACMModel);
		strMACMModel = strOrderedMACMModel.substring(0, strOrderedMACMModel.lastIndexOf(","));
		strMACMModel = "CREATE\n" + strMACMModel;
		return (strMACMModel);

	}

	/**
	 * This method is called to establish a relation between an agent an an
	 * application component in the database
	 * 
	 * @param agentRelationAppId
	 * @param agentRelationComponentId
	 * @param agentRelationAgentId
	 * @return
	 */
	@PUT
	@Path("/setAgentRelation/{agentRelationAppId}/{agentRelationComponentId}/{agentRelationAgentId}")
	@Produces("text/plain")
	public String setAgentRelation(@PathParam("agentRelationAppId") String agentRelationAppId,
			@PathParam("agentRelationComponentId") String agentRelationComponentId,
			@PathParam("agentRelationAgentId") String agentRelationAgentId) {
		String res = "true";
		ManageMCApp base = new ManageMCApp();
		base.setApplicationComponentAgentRelation(agentRelationAppId, agentRelationAgentId, agentRelationComponentId);
		return (res);

	}

}
