package eu.musa.modeller.ws;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;



/**
 * REST Web Service
 *
 * @author 106363
 */
@Path("PruebaService")
public class PruebaService {

    @GET
    @Path("prueba")
    @Produces("application/json")
    public String prueba () {
    	return "Prueba de servicio correcta!!";

    }



    @GET
    @Path("pruebaServiceOtherDomain")
    @Produces("application/json")
    public String pruebaServiceOtherDomain () {
    	String result = "";
    	try {

    		URL url = new URL("http://modeller.musa-project.eu/eu.musa.modeller.ws/webresources/camelfilews/getCamel/120/MACM");
    		//URL url = new URL("http://localhost:8080/eu.musa.modeller.ws/webresources/PruebaService/prueba");
    		
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    		conn.setRequestMethod("GET");
    		conn.setRequestProperty("Accept", MediaType.TEXT_PLAIN);

    		if (conn.getResponseCode() != 200) {
    			throw new RuntimeException("Failed : HTTP error code : "
    					+ conn.getResponseCode());
    		}

    		BufferedReader br = new BufferedReader(new InputStreamReader(
    			(conn.getInputStream())));

    		
    		System.out.println("Output from Server .... \n");
    		String output;
    		while ((output = br.readLine()) != null) {
    			System.out.println(output);
    			result+=output;
    		}

    		conn.disconnect();

    	  } catch (MalformedURLException e) {

    		e.printStackTrace();

    	  } catch (IOException e) {

    		e.printStackTrace();

    	  }
    	return result;

    	}

    }



